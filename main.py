import sys
import random
import scripts.simulation_scripts.fpt_evaluation as fpt_evaluation
import scripts.simulation_scripts.positioning_evaluation as positioning_evaluation
import scripts.evolution_scripts.GeneticAlgorithm as GA
import scripts.evolution_scripts.post_evaluation as post_evaluation
import scripts.analyze_scripts.ga_evolution_curve as plot_evocurve
import scripts.analyze_scripts.network_behaviour_analysis as network_behaviour_analysis
import scripts.analyze_scripts.statistical_significance as stats_significance
import scripts.analyze_scripts.strategies_comparasion as plot_strategies
import scripts.analyze_scripts.kilobot_trajectory as kilobot_trajectory
import scripts.analyze_scripts.plot_network_behaviour as plot_network_behaviour

if __name__ == "__main__":
    if len(sys.argv) <= 1:
        print("Not enough arguments!")
    else:
        script_name = str(sys.argv[1])
        try:
            if script_name == "GA":
                num_threads = int(sys.argv[2])
                seeds = int(sys.argv[3])
                print(f"Running {seeds} seed(s) for Genetic Algorithm.")

                for seed in range(seeds):
                    print("Running seed number %d." % (seed+1))
                    GA.run_evolution(num_threads, seed)
                print(f'All {seeds} seeds finished! Experiment complete.')

            elif script_name == "posteva":
                num_threads = int(sys.argv[2])
                post_evaluation.runPostEvaluation(num_threads)

            elif script_name == "plot_evocurve":
                plot_evocurve.main()

            elif script_name == "fpt_evaluation":
                num_threads = int(sys.argv[2])
                strategy = sys.argv[3]
                fpt_evaluation.runFptEvaluation(num_threads, strategy)

            elif script_name == "positioning_evaluation":
                num_threads = int(sys.argv[2])
                strategy = sys.argv[3]
                positioning_evaluation.runPositioningEvaluation(num_threads, strategy)

            elif script_name == "stats_significance":
                stats_significance.main()

            elif script_name == "chaos":
                bn_type = sys.argv[2]
                num_threads = int(sys.argv[3])
                network_behaviour_analysis.main(bn_type, num_threads)

            elif script_name == "plot_chaos":
                bn_type = sys.argv[2]
                plot_network_behaviour.main(bn_type)

            # elif script_name == "net_behaviour":
            #     network_behaviour.main()

            elif script_name == "plot_strategies":
                plot_type = sys.argv[2]
                if plot_type == "fpt":
                    plot_strategies.showStrategiesEvaluation()
                elif plot_type == "pos":
                    kilobot_trajectory.trajectoryAnalisys()

            else:
                print("Script option doesnt exist!")

        except Exception as e:
            print(f"(main.py) Error: {e}")

            
        


import os
import matplotlib.pyplot as plt
import csv
import scripts.plot_functions as plot_functions
from scipy.optimize import curve_fit
from scipy.ndimage.filters import uniform_filter1d
from scipy.signal import savgol_filter
import scripts.io_scripts as io_scripts

evolution_main_dir = "Data/Evolution Data/Evolutions"
posteva_main_dir = "Data/Evolution Data/Post-evaluation/GA_Best_EBNs"

def plotEvolutionCurveComparision(num_robots, num_nodes, date, target_type):
    evo_data = io_scripts.readEBNsfromEvolutionFolder(evolution_main_dir, num_robots, num_nodes, date, target_type)

    for ebn_results in evo_data:
        disc_weibull = uniform_filter1d(ebn_results["weibull_disc"], size=10)
        # disc_weibull = savgol_filter(disc_weibull, 9, 1)
        ebn_results["weibull_filtered"] = disc_weibull
        disc_fraction = uniform_filter1d(ebn_results["frac_disc"], size=10)
        # disc_weibull = savgol_filter(disc_weibull, 9, 1)
        ebn_results["frac_disc_filtered"] = disc_fraction 

    title = "GA Evolution Curve for Fraction Discovery (%sR - %sN)" % (num_robots, num_nodes)
    xlabel = "Generation"
    ylabel ="First Passage Time"
    results = [data["weibull_disc"] for data in evo_data]
    legends = [data["dir_name"] for data in evo_data]
    
    print("Ploting Evolution Curve:")
    plot_functions.plotCurveComparison(results, legends, title, xlabel, ylabel)

def plotEvolutionCurveAfterPostEva(num_robots, num_nodes, date):
    all_data = []
    all_dirs = os.listdir(posteva_main_dir)
    all_dirs.sort()
    for posteva_dir in all_dirs:
        file_robots = "-1"
        file_nodes = "-1"
        file_date = ""
        algorithm_name = ""
        elements = posteva_dir.split("_")
        algorithm_name = elements[0]
        for e in elements:
            if (e.startswith("202")):
                file_date = e
            if e.endswith("R"):
                file_robots = int(e[:-1])
            if (e.endswith("N")):
                file_nodes = int(e[:-1])

        if (algorithm_name != "ga"):
            continue

        # Check specific number of node and robots
        if (file_robots != num_robots or file_nodes != num_nodes):
            continue

        # Check date of experiments
        if ((not file_date.startswith(date))):
            continue

        print(posteva_dir)
        disc_weibull = []
        disc_time = []
        frac_disc = []
        inf_time = []
        frac_inf = []
        posteva_file_path = posteva_main_dir + "/" + posteva_dir+ "/posteva_all-gen_best_ebns.txt"
        if os.path.exists(posteva_file_path):
            with open(posteva_file_path, 'r') as data:
                try:
                    lines = data.readlines()
                    for line in lines:
                        if line.startswith("fitness:"):
                            disc_weibull.append(int(line.split()[1])/32.0)
                            disc_time.append(int(line.split()[2]))
                            frac_disc.append(float(line.split()[3]))
                            inf_time.append(int(line.split()[4]))
                            frac_inf.append(float(line.split()[5]))
                    data.close()
                except Exception as e:
                    print("Couldnt open data file!\n" + str(e))
        else:
            continue

        # weight_curve_mean = uniform_filter1d(disc_weibull, size=10)
        # weight_curve_mean = savgol_filter(disc_weibull, 10)
        all_data.append((disc_weibull, posteva_dir))

    title = "GA Evolution Curve after Post-evaluation (%sR - %sN)" % (num_robots, num_nodes)
    xlabel = "Generation"
    ylabel ="First Passage Time"
    results = [data[0] for data in all_data]
    legends = [data[1] for data in all_data]

    plot_functions.plotCurveComparison(results, legends, title, xlabel, ylabel)

def plotComparisonWithPostEvaEvoCurve(num_robots, num_nodes, date):
    all_data = []

    all_dirs = os.listdir(posteva_main_dir)
    all_dirs.sort()
    for posteva_dir in all_dirs:
        file_robots = "-1"
        file_nodes = "-1"
        file_date = ""
        algorithm_name = ""
        elements = posteva_dir.split("_")
        algorithm_name = elements[0]
        for e in elements:
            if (e.startswith("202")):
                file_date = e
            if e.endswith("R"):
                file_robots = int(e[:-1])
            if (e.endswith("N")):
                file_nodes = int(e[:-1])

        if (algorithm_name != "ga"):
            continue

        # Check specific number of node and robots
        if (file_robots != num_robots or file_nodes != num_nodes):
            continue

        # Check date of experiments
        if ((not file_date.startswith(date))):
            continue

        disc_weibull = []
        disc_time = []
        frac_disc = []
        inf_time = []
        frac_inf = []
        posteva_file_path = posteva_main_dir + "/" + posteva_dir+ "/posteva_all-gen_best_ebns.txt"
        if os.path.exists(posteva_file_path):
            print(posteva_dir)
            with open(posteva_file_path, 'r') as data:
                try:
                    lines = data.readlines()
                    for line in lines:
                        if line.startswith("fitness:"):
                            disc_weibull.append(int(line.split()[1])/32.0)
                            disc_time.append(int(line.split()[2]))
                            frac_disc.append(float(line.split()[3]))
                            inf_time.append(int(line.split()[4]))
                            frac_inf.append(float(line.split()[5]))
                    data.close()
                except Exception as e:
                    print("Couldnt open data file!\n" + str(e))
        else:
            continue

        weight_curve_mean = uniform_filter1d(disc_weibull, size=5)
        # weight_curve_mean = savgol_filter(disc_weibull, 10)
        all_data.append((weight_curve_mean, posteva_dir))

    all_dirs = os.listdir(evolution_main_dir)
    all_dirs.sort()
    for evolution_dir in all_dirs:
        file_robots = "-1"
        file_nodes = "-1"
        file_date = ""
        algorithm_name = ""
        elements = evolution_dir.split("_")
        algorithm_name = elements[1]
        for e in elements:
            if (e.startswith("202")):
                file_date = e
            if e.endswith("R"):
                file_robots = int(e[:-1])
            if (e.endswith("N")):
                file_nodes = int(e[:-1])

        if (algorithm_name != "ga"):
            continue

        # Check specific number of node and robots
        if (file_robots != num_robots or file_nodes != num_nodes):
            continue

        # Check date of experiments
        if ((not file_date.startswith(date))):
            continue

        if (file_date != "2023-03-01" and file_date != "2023-03-10"):
            continue

        print(evolution_dir)
        disc_weibull = []
        disc_time = []
        frac_disc = []
        inf_time = []
        frac_inf = []
        evolution_curve_path_txt = evolution_main_dir + "/" + evolution_dir + "/evolution_curve_data.txt"
        if os.path.exists(evolution_curve_path_txt):
            with open(evolution_curve_path_txt, 'r') as data:
                try:
                    lines = data.readlines()
                    for line in lines:
                        disc_weibull.append(int(line.split()[0])/32.0)
                        disc_time.append(int(line.split()[1]))
                        frac_disc.append(float(line.split()[2]))
                        inf_time.append(int(line.split()[3]))
                        frac_inf.append(float(line.split()[4]))
                    data.close()
                except Exception as e:
                    print("Couldnt open data file!\n" + str(e))
        else:
            with open(evolution_main_dir + "/" + evolution_dir + "/evolution_curve_data.tsv", 'r') as data:
                try:
                    read_tsv = csv.reader(data, delimiter="\t")
                    next(read_tsv, None)
                    for row in read_tsv:
                        disc_weibull.append(float(row[1])/32.0)
                        disc_time.append(float(row[2])/32.0)
                        frac_disc.append(float(row[3]))
                        inf_time.append(float(row[4])/32.0)
                        frac_inf.append(float(row[5]))
                    data.close()
                except Exception as e:
                    print("Couldnt open data file!\n" + str(e))

        weight_curve_mean = uniform_filter1d(disc_weibull, size=5)
        # weight_curve_mean = savgol_filter(disc_weibull, 10)
        all_data.append((weight_curve_mean, evolution_dir))

    title = "GA Evolution Curve comparison with Post-evaluation (%sR - %sN)" % (num_robots, num_nodes)
    xlabel = "Generation"
    ylabel ="First Passage Time"
    results = [data[0] for data in all_data]
    legends = [data[1] for data in all_data]
    print("Ploting Evolution Curve comparison with post-evaluation:")
    plot_functions.plotCurveComparison(results, legends, title, xlabel, ylabel)

def main():
    num_robots = 20
    date = "202"
    target_type = "random-targets"

    all_nodes = [18]
    for node in all_nodes:
        plotEvolutionCurveComparision(num_robots, node, date, target_type)
        # plotEvolutionCurveAfterPostEva(num_robots, num_nodes, date)
        # plotComparisonWithPostEvaEvoCurve(num_robots, num_nodes, date)
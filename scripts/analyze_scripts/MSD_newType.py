#!/usr/bin/env python
# coding: utf-8

from IPython.core.display import display, HTML
display(HTML("<style>.container { width:80% !important; }</style>"))
from termcolor import colored
import numpy as np
import os
import pandas as pd
import matplotlib.pyplot as plt
from natsort import natsorted
import utils

# Display pandas df without truncation
pd.set_option('display.max_rows', None)
pd.set_option('display.max_columns', None)
pd.set_option('display.width', None)
pd.set_option('display.max_colwidth', None)

# Globally setting font sizes via rcParams should be done with 
params = {
    'font.size': 11,
    'figure.figsize': (10,3),
    'figure.dpi': 80,
    'savefig.dpi': 300,
    'legend.fontsize': 11,
    'xtick.labelsize': 11,
    'ytick.labelsize': 11,
    'axes.labelsize': 11,
    'axes.axisbelow': True
         }
plt.rcParams.update(params)

# The defaults can be restored using
# plt.rcParams.update(plt.rcParamsDefault)

# --------------------------------------------------------------------------- #

def createNumpyFiles(tsv_folder, np_folder):
    for dirName, subdirList, fileList in os.walk(tsv_folder):
        # print(dirName)
        num_robots = "-1"
        num_nodes = "-1"
        samplep = ""
        rho = 0
        elements = dirName.split("_")
        for e in elements:
            if (e.startswith("samplep")):
                samplep = e.split("#")[-1]
            if e.startswith("robots"):
                num_robots = e.split("#")[-1]
            if (e.startswith("nodes")):
                num_nodes = e.split("#")[-1]
            if e.startswith("rho"):
                rho = float(e.split("#")[-1])
            if e.startswith("alpha"):
                alpha = float(e.split("#")[-1])

        if (num_robots == "-1") or (samplep == ""):
            continue

        df_experiment = pd.DataFrame()
        [number_of_experiments, df_experiment] = utils.load_pd_positions(dirName, "experiment")
        positions_concatenated = df_experiment.values[:, 1:1800]
        [num_robot, num_times] = positions_concatenated.shape
        positions_concatenated = np.array([x.split(',') for x in positions_concatenated.ravel()], dtype=float)
        positions_concatenated = positions_concatenated.reshape(num_robot, num_times, 2)
        pos_transposed = positions_concatenated.transpose(1,0,2)
        # print(pos_transposed)
        if num_nodes == "-1":
            np.save(np_folder +'/positions_CRWLEVY_robots#20_alpha#%s_rho#%s.npy' % (alpha, rho), pos_transposed)
        else:
            np.save(np_folder + '/positions_EBN_robots#20_nodes#%s.npy' % (num_nodes), pos_transposed)

def evaluate_MSD(main_folder, folder_experiments):
    msd_dict = {}
    windows = np.array([10, 20, 30, 40, 50, 60, 90, 100, 120, 150, 180, 200, 300, 450, 600])
    
    for dirName, subdirList, fileList in os.walk(os.path.join(main_folder, folder_experiments)):
        print(colored("DirName:", 'blue'), dirName)
        for fileName in natsorted(fileList):
            if 'real' in fileName:
                exp_type = 'real'
                continue
            else:
                exp_type = 'simulated'

            num_robots = -1
            alpha = "2.0"
            rho = "-1"
            nodes = "-1"
            print(colored("\tfileName:", 'blue'), fileName)
            elements = fileName.replace('.npy', '').split('_')
            for e in elements:
                if e.startswith('robots'):
                    num_robots = int(e.split('#')[1])
                if e.startswith('alpha'):
                    alpha = e.split('#')[1]
                if e.startswith('rho'):
                    rho = e.split('#')[1]
                if e.startswith('nodes'):
                    nodes = e.split('#')[1]

            if num_robots == -1:
                print('Error!!! num_robots not a right value')
                continue

            if nodes in ["18","26"]:
                continue
            if nodes == "-1" and rho != "0.75":
                continue
            alpha = r'$\alpha$:2 $\rho$:0.75'

            # if alpha == "-1" or rho == "-1":
            #     print('Error!!! Alpha and/or Rho dont have valid values')
            #     continue

            if num_robots not in msd_dict:
                msd_dict[num_robots] = {}
            # if alpha not in msd_dict[num_robots]:
            #     msd_dict[num_robots][alpha] = {}
            # if nodes not in msd_dict[num_robots]:
            #     msd_dict[num_robots][nodes] = {}
            # print('\tnum_robots: {}, alpha: {}, rho: {}'.format(num_robots, alpha, rho))
            # else:
            #     if num_robots not in msd_dict:
            #         msd_dict[num_robots] = {}
            #     if nodes not in msd_dict[num_robots]:
            #         msd_dict[num_robots][nodes] = {}
            #     print('\tnum_robots: {}, nodes: {}'.format(num_robots, nodes))

            np_position = np.load(os.path.join(main_folder, folder_experiments, fileName))
            print('\tnp_position.shape:', np_position.shape)

            msd = np.array([])
            for window in windows:
                # print('\t window: {}'.format(window))
                msd_matrix = np.array([])
                for f in range(window, np_position.shape[0], window):
                    # print('tf: {}, ti: {}'.format(f, f - window))
                    tf = np_position[f]
                    ti = np_position[f - window]
                    # print('tf.shape:', tf.shape)
                    sq_distance = np.sum((tf - ti) ** 2, axis=1)

                    msd_matrix = np.row_stack([msd_matrix, sq_distance]) if msd_matrix.size else sq_distance
                msd = np.append(msd, np.mean(msd_matrix))

            if nodes == "-1":
                msd_dict[num_robots][alpha] = msd
            else:
                msd_dict[num_robots][nodes] = msd
            
    return msd_dict

def plot_MSD(msd_dict, baseline_msd_dict, top_v, saveFig=False, figName=''):
    # colors = ['red','blue','darkgreen','crimson','turquoise', 'khaki','navy', 'orangered', 'sienna']
    Ncolors = 4
    colormap = plt.cm.viridis  # LinearSegmentedColormap
    Ncolors = min(colormap.N, Ncolors)
    mapcolors = [colormap(int(x * colormap.N / Ncolors)) for x in range(Ncolors)]

    # for n_rob in msd_dict:
    #     for alpha in msd_dict[n_rob]:
    fig, ax = plt.subplots()
    ax.grid(True,linestyle='--',color='0.7')
    # x_axis = np.arange(windows.size)
    x_axis = windows

    # for idx,rho in enumerate(msd_dict[n_rob][alpha]):
    #     print(msd_dict[n_rob][alpha])
    #     y_axis = msd_dict[n_rob][alpha][rho]
    #     ax.plot(x_axis, y_axis, marker='o', label=rho, color=mapcolors[idx])
        
    # if alpha != '1.4' and alpha != '1.8':
    #     y_base_axis = baseline_msd_dict[1][alpha][rho]
    #     ax.plot(x_axis, y_base_axis, linestyle='dashed', color=mapcolors[idx])

    labels = ["LMCRW: " + r'$\alpha$:2 $\rho$:0.75',"EBN: 22N", "EBN: 30N"]
    for idx, robots in enumerate(msd_dict):
        for i,s in enumerate(msd_dict[robots]):
            y_axis = msd_dict[robots][s]
            ax.plot(x_axis, y_axis, marker='o', label=labels[i], color=mapcolors[i])
    
    if top_v:
        plt.ylim((0,top_v))
    else:
        # plt.ylim((0,np.max(y_axis)+0.1))
        plt.ylim((0,0.3))

    plt.xticks(ticks=x_axis, labels=windows)
    plt.title('{}'.format(figName))
    # plt.title('{} - num_robots: {}'.format(figName, "20"))
    # plt.title('{} - num robots: {}; alpha: {}'.format(figName, n_rob, a))
    # plt.legend(title="rho",loc=2)
    plt.legend(title="Strategies",loc=2)
    plt.tight_layout()
    
    # if saveFig:
    #     plt.savefig(figName+'_num_robots#{}_alpha#{}.png'.format(n_rob, ))
    plt.show()

def plotMSDComparingBias(msd_bias_dict, msd_no_bias_dict, baseline_msd_dict, top_v):
    # colors = ['red','blue','darkgreen','crimson','turquoise', 'khaki','navy', 'orangered', 'sienna']
    Ncolors = 4
    colormap = plt.cm.viridis  # LinearSegmentedColormap
    Ncolors = min(colormap.N, Ncolors)
    mapcolors = [colormap(int(x * colormap.N / Ncolors)) for x in range(Ncolors)]

    fig, ax = plt.subplots(2,1, sharex=True)
    ax[0].grid(True,linestyle='--',color='0.7')
    ax[1].grid(True,linestyle='--',color='0.7')
    x_axis = windows

    labels = ["LMCRW: " + r'$\alpha$:2 $\rho$:0.75',"EBN: 22N", "EBN: 30N"]
    for i, msd_dict in enumerate([msd_bias_dict, msd_no_bias_dict]):
        for j, robots in enumerate(msd_dict):
            for k, strategy in enumerate(msd_dict[robots]):
                y_axis = msd_dict[robots][strategy]
                ax[i].plot(x_axis, y_axis, marker='o', label=labels[k], color=mapcolors[k])
    
        if top_v:
            ax[i].set_ylim((0,top_v))
        else:
            # plt.ylim((0,np.max(y_axis)+0.1))
            ax[i].set_ylim((0,0.3))
        ax[i].set_xticks(ticks=x_axis)
        ax[i].legend(title="Strategies",loc=2)

    ax[0].set_title("Bias Experiment")
    ax[1].set_title("No Bias Experiment")
    # fig.suptitle("MSD comparison between LMCRW and EBN (w/ and not Bias)")
    # plt.title('{} - num_robots: {}'.format(figName, "20"))
    # plt.title('{} - num robots: {}; alpha: {}'.format(figName, n_rob, a))
    # plt.legend(title="rho",loc=2)
    # plt.tight_layout()
    
    # if saveFig:
    #     plt.savefig(figName+'_num_robots#{}_alpha#{}.png'.format(n_rob, ))
    plt.xlabel("Window")
    plt.show()

main_folder = os.getcwd() #attenzione pycharm scprits not in the same dir
baseline_folder = 'results/ALL/pos_pkl/Simulated/baseline'
simpleExperiment_folder = 'results/ALL/pos_pkl/Simulated/simple_experiment_100_runs'
biasExperiment_folder = 'results/ALL/pos_pkl/Simulated/bias_experiment_100_runs'
randomAngle_folder = 'results/ALL/pos_pkl/Simulated/random_angle_100_runs'
bouncingAngle_folder = 'results/ALL/pos_pkl/Simulated/bouncing_angle_100_runs'

# createNumpyFiles("../Data/EBN position data/No Bias", simpleExperiment_folder)
# createNumpyFiles("../Data/EBN position data/Original Bias", biasExperiment_folder)

windows = np.array([1, 2, 3, 4, 5, 6, 9, 10, 12, 15, 18, 20, 30, 45, 60])
baseline_dict = evaluate_MSD(main_folder, baseline_folder)
simpleExperiment_dict = evaluate_MSD(main_folder, simpleExperiment_folder)
biasExperiment_dict = evaluate_MSD(main_folder, biasExperiment_folder)
# randomAngle_dict = evaluate_MSD(main_folder, randomAngle_folder)
# bouncingAngle_dict = evaluate_MSD(main_folder, bouncingAngle_folder)

plotMSDComparingBias(biasExperiment_dict, simpleExperiment_dict, baseline_dict, top_v=0.3)
# plot_MSD(simpleExperiment_dict, baseline_dict, top_v=0.3, saveFig=True, figName='Bias Experiment')
# plot_MSD(biasExperiment_dict, baseline_dict, top_v=0, saveFig=True, figName='No Bias Experiment')
# plot_MSD(randomAngle_dict, baseline_dict, top_v=0.3, saveFig=True, figName='randomAngle_')
# plot_MSD(bouncingAngle_dict, baseline_dict, top_v=0.3, saveFig=True, figName='bouncingAngle_')







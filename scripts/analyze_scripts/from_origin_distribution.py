from turtle import pos
import matplotlib.pyplot as plt
import numpy as np
import csv
import sys
import os
from scipy import stats
import math
from scipy.optimize import curve_fit
from scipy.stats import norm
import seaborn as sns
import pandas as pd
import powerlaw
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter,
                               AutoMinorLocator)
import utils
import time

dir_folder = "../Data/EBN position data/"
result_folder = "../results/"

def checkNoneValue(list_conc):
    count = 0
    for x in list_conc.ravel():
        if isinstance(x, float):
            count += 1
    if count > 0:
        print("There were %d NaN values." % (count))

def distance_from_origin_distribution(main_folder, result_folder):
    Ncolors = 200
    colormap = plt.cm.viridis  # LinearSegmentedColormap
    #     print("colormap.N", colormap.N)
    Ncolors = min(colormap.N, Ncolors)
    mapcolors = [colormap(int(x * colormap.N / Ncolors)) for x in range(Ncolors)]
    # print(len(mapcolors))

    for dirName, subdirList, fileList in os.walk(main_folder):
        for subdir in subdirList:
            num_robots = "-1"
            num_nodes = "-1"
            encoded = ""
            elements = subdir.split("_")
            for e in elements:
                if (e.startswith("encoded")):
                    encoded = "encoded"
                if e.startswith("robots"):
                    num_robots = e.split("#")[-1]
                if (e.startswith("nodes")):
                    num_nodes = e.split("#")[-1]

            if (num_robots == "-1" or num_nodes == "-1"):
                continue

            df_experiment = pd.DataFrame()
            [_, df_experiment] = utils.load_pd_positions(dirName+subdir, "experiment")

            #     print(number_of_experiments)
            positions_concatenated = df_experiment.values[:, 1:200]
            [num_robot, num_times] = positions_concatenated.shape
            print(positions_concatenated.shape)
            checkNoneValue(positions_concatenated)
            positions_concatenated = np.array([x.split(',') for x in positions_concatenated.ravel()], dtype=float)
            positions_concatenated = positions_concatenated.reshape(num_robot, num_times, 2)
            print(positions_concatenated.shape)

            distances = utils.distance_from_the_origin(positions_concatenated)
            print("distances.shape", distances.shape)
            fig = plt.figure(figsize=(10, 5), dpi=160)
            plt.xlim((0.001, 100))
            plt.ylim((0.0001, 100))
            print(len(distances))
            for i, d in enumerate(distances):
                fit = powerlaw.Fit(d, xmin=0.00001)
                # print(i)
                fit.plot_pdf(linewidth=2, color=mapcolors[i])
            plt.ylabel('p(x)')
            plt.xlabel('distance from origin')
            plt.title("origin distance distribution with %s robots, %s Nodes %s" % (num_robots, num_nodes, encoded))
            # file_name = "powerlaw_%s_N_%s_%s_robots.png" % (num_nodes, encoded, num_robots)
            # plt.savefig(result_folder + file_name)
            # plt.close(fig)
            plt.show()

distance_from_origin_distribution(dir_folder, result_folder)

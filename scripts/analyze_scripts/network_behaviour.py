import os
from matplotlib import projections
import matplotlib.pyplot as plt
from matplotlib.collections import LineCollection
from mpl_toolkits import mplot3d
import numpy as np
import random
import src.utils.BooleanNetwork as BooleanNetwork
import scripts.utils as utils

ebn_folder = "Data/Evolution Data/Post-evaluation/GA_Best_EBNs/"
rbn_folder = "Data/Network Parameters Data/RBNs parameters/"
# robots_initial_states_file = "data/initial_states_ebn_20R_20N.txt"

def readEBNsParameters(num_nodes, num_robots, main_folder, experiment_date):
    already_read = []
    networks = []
    for subdir, dirs, files in os.walk(main_folder):
        dirs.sort()
        for folder in dirs:
            elements = folder.split("_")
            date = ""
            file_nodes = -1
            file_robots = -1
            for e in elements:
                if e.endswith("N"):
                    file_nodes = int(e.replace("N", ""))
                if e.endswith("R"):
                    file_robots = int(e.replace("R", ""))
                if e.startswith("2023"):
                    date = e  

            if file_nodes not in num_nodes or file_robots != num_robots:
                continue

            if file_nodes not in already_read:
                already_read.append(file_nodes)
            else:
                continue
            
            # if not date.startswith(experiment_date):
            #     continue

            init_state_files = ""
            parameters_files = ""
            for file_name in os.listdir(main_folder + folder):
                if file_name == "initial_states.txt" or file_name == "initial-states.txt":
                    init_state_files = main_folder + folder + "/" + file_name
                if file_name == "parameters_best_ebn.txt":
                    parameters_files = main_folder + folder + "/" + file_name

            ebn = BooleanNetwork.BooleanNetwork(file_nodes, bn_type="EBN", read_from=parameters_files)
            ebn.readRobotsInitialStates(file_robots, init_state_files)
            networks.append(ebn)

    return networks

def plotNetworkValuesOverTime(node_decimal_values, num_nodes):
    fig, ax = plt.subplots()
    ax.plot(node_decimal_values)
    ax.set_title(f"EBN {num_nodes}N values over time")    
    ax.set_xlabel('step')
    ax.set_ylabel('value')
    fig.set_size_inches(16, 9)
    plt.show()

def plotRBNAttractor2D(node_decimal_values, num_nodes):
    fig, ax = plt.subplots()

    x = np.array(node_decimal_values[:-1])
    y = np.array(node_decimal_values[1:])
    # cols = np.linspace(0,1,len(x))

    # points = np.array([x, y]).T.reshape(-1, 1, 2)
    # segments = np.concatenate([points[:-1], points[1:]], axis=1)
    
    # lc = LineCollection(segments, cmap='inferno')
    # lc.set_array(cols)
    # lc.set_linewidth(2)
    # line = ax.add_collection(lc)
    # fig.colorbar(line,ax=ax)

    ax.plot(x, y, label='RBN %d attractor' % (num_nodes))
    ax.legend()  
    ax.set_xlabel('x')
    ax.set_ylabel('x+1')
    # ax.set_title("RBN node values over time")  
    fig.set_size_inches(16, 9)
    plt.show()

def plotRBNAttractor3D(node_decimal_values, num_nodes):
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    x = np.array(node_decimal_values[:-2])
    y = np.array(node_decimal_values[1:-1])
    z = np.array(node_decimal_values[2:])

    ax.plot(x, y, z, label='RBN %d attractor' % (num_nodes))
    ax.legend()  
    ax.set_xlabel('x')
    ax.set_ylabel('x+1')
    ax.set_zlabel('x+2')
    # ax.set_title("RBN node values over time")  
    fig.set_size_inches(16, 9)
    plt.show()

def plotComparisonNodeStates(ebns, rbns, max_steps):
    nodes = [20,22,30]
    fig = plt.figure(figsize=(9, 6))
    # fig.suptitle('Figure title')

    subfigs = fig.subfigures(1, 3, wspace=0.07)
    for i, subfig in enumerate(subfigs):
        subfig.suptitle("%dN" % (nodes[i]), fontsize=18, y=0.95)
        axs = subfig.subplots(nrows=1, ncols=2)
        bn = [rbns[i], ebns[i]]
        for j, ax in enumerate(axs):
            generation_1 = []
            node_1 = []
            generation_2 = []
            node_2 = []
            gen = max_steps
            for step in bn[j].node_states[:gen]:
                for n in range(len(step)):
                    if step[n] == 1:
                        node_1.append(n)
                        generation_1.append(gen)
                    else:
                        node_2.append(n)
                        generation_2.append(gen)
                gen -= 1

            # generation_1.reverse()
            # generation_2.reverse()
            ax.scatter(node_1, generation_1, c="#ffffff", s=10,marker="s")
            ax.scatter(node_2, generation_2, c="#3b3b3c", s=10,marker="s")
            ax.set_xticks([])
            ax.set_yticks([])
            ax.set_xlabel("%s" % (bn[j].bn_type), fontsize=18)

    # fig.show()        
    # plt.title("%s %dN" % (rbn.bn_type, rbn.num_nodes))
    # plt.xlabel("Nodes")
    # plt.ylabel("Time Steps (t)")
    # ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    # # plt.xticks(range(math.floor(min(node_1)), math.ceil(max(node_1))+1))
    # fig.subplots_adjust(bottom=0.01)
    # plt.tight_layout()
    # plt.gca().invert_yaxis()
    # ax.get_xaxis().set_visible(False)
    plt.show()

def main():
    random.seed(1)

    num_nodes = [20,22,30]
    num_robots = 20
    num_max_steps = 100
    max_sim_time = 3000
    num_population = 5
    num_initial_states = 10
    num_connections = 25
    min_connections = 1

    rbns = []
    rbns = [BooleanNetwork.BooleanNetwork(n, bn_type="RBN") for n in num_nodes]
    for net in rbns:
        net.createRobotsInitialStates(num_robots)
    ebns = readEBNsParameters(num_nodes, num_robots, ebn_folder, 2023)

    rbns_node_states = []
    for rbn in rbns:
        rbn.generateInitialState()
        node_states, node_decimal_values = rbn.getNetworkStatesOverTimeSteps(num_max_steps)
        print(f"RBN {rbn.num_nodes}N - mean SL = {rbn.getAvgSLUntilSimulationEnd(3000)}")
    ebns_node_states = []
    for ebn in ebns:
        ebn.generateInitialState()
        node_states, node_decimal_values = ebn.getNetworkStatesOverTimeSteps(num_max_steps)
        print(f"EBN {ebn.num_nodes}N - mean SL = {ebn.getAvgSLUntilSimulationEnd(3000)}")
    plotComparisonNodeStates(ebns, rbns, num_max_steps)

    # for i in range(num_initial_states):
    #     for network in networks:
    #         # node_states, node_decimal_values = network.getNetworkStatesOverSimTime(max_sim_time)
    #         network.generateInitialState()
    #         node_states, node_decimal_values = network.getNetworkStatesOverTimeSteps(num_max_steps)
            # plotNetworkValuesOverTime(node_decimal_values, network.num_nodes)
            # plotRBNAttractor2D(node_decimal_values, network.num_nodes)
            # plotRBNAttractor3D(node_decimal_values, num_nodes)

    # population = []
    # for i in range(num_population):
    #     # population.append(BooleanNetwork.BooleanNetwork(num_nodes))
    #     population.append(BooleanNetwork.BooleanNetwork(num_nodes, K=num_connections))

    # for network in population:
    #     for i in range(num_initial_states):
    #         network.generateInitialState()
    #         node_states, node_decimal_values = network.getNetworkStatesOverTime(num_max_steps)
    #         # plotNetworkValuesOverTime(node_decimal_values)
    #         plotRBNAttractor2D(node_decimal_values, num_nodes)
    #         plotRBNAttractor3D(node_decimal_values, num_nodes)
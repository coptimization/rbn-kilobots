import os
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter
import numpy as np
import scipy.stats as stats
import pandas as pd
import seaborn as sns
import scripts.io_scripts as io_scripts
import scripts.utils as utils
import scripts.plot_functions as plot_func
from sympy import true

data_folder = "Data/Weibull Discovery time data/default arena/"
arena_sizes_folder = "Data/Weibull Discovery time data/different_arena_sizes/"

def selectExperiment(experiment_parameters, comparison, strategies, num_robots, nodes, alpha, rho, trials, bias):
    if experiment_parameters['strategy'] not in strategies:
        return False

    if experiment_parameters['robots'] not in num_robots:
        return False
    
    if experiment_parameters['bias'] != bias:
        return False

    if comparison == "Many Comparision":
        if experiment_parameters['encode'] != "direct":
            return False
        if experiment_parameters['strategy'] == "crwlevy":
            if experiment_parameters['alpha'] != alpha or experiment_parameters['rho'] != rho:
                return False
            if experiment_parameters['evaluations'] == 1:
                return False
        else:
            if experiment_parameters['nodes'] not in nodes:
            # if experiment_parameters['nodes'] not in nodes and experiment_parameters['strategy'] == "rbn":
                return False
            if experiment_parameters['evaluations'] != 20:
                return False
            if experiment_parameters['strategy'] == "ebn":
                if experiment_parameters['num_net'] < 2:
                    return False
                if experiment_parameters['id'] == "fixed-targets":
                    return False
            else:
                if experiment_parameters['num_net'] != 100:
                    return False
        if experiment_parameters['trials'] != trials:
            return False
        
    elif comparison == "Encoded":
        if experiment_parameters['nodes'] not in nodes:
            return False
        # if experiment_parameters['nodes'] in nodes:
        #     if experiment_parameters['nodes'] not in [22,30] and not experiment_parameters['encode']== "indirect":
        #         return False
        # else:
        #     return False
        if experiment_parameters['encode'] == "direct":
            if experiment_parameters['evaluations'] != 20 or experiment_parameters['num_net'] != 100:
                return False
        elif experiment_parameters['encode'] == "20N-encoded":
            if experiment_parameters['evaluations'] != 20 or experiment_parameters['num_net'] != 100:
                return False
        else:
                return False

    elif comparison == "Arena Sizes":
        if experiment_parameters['encode'] == "indirect":
            return False
        if experiment_parameters['nodes'] not in nodes:
            return False
        if experiment_parameters['num_net'] != 100:
            return False
        if experiment_parameters['arena'] == 45 and experiment_parameters['robots'] != 5:
            return False
        if experiment_parameters['arena'] == 90 and experiment_parameters['robots'] != 20:
            return False
        if experiment_parameters['arena'] == 180 and experiment_parameters['robots'] != 80:
            return False
        # if experiment_parameters['robots'] == 10:
        #     return False

    elif comparison == "Bias Comparision":
        if experiment_parameters['encode'] == "indirect":
            return False
        if experiment_parameters['strategy'] == "crwlevy":
            if experiment_parameters['alpha'] != alpha:
                return False
        else:
            if experiment_parameters['nodes'] not in nodes:
                return False
            if experiment_parameters['trials'] != trials:
                return False
            if experiment_parameters['num_net'] != 100:
                return False
            
    else:
        print(f"{comparison} experiment type doesnt exist!")
        exit(0)

    return True

def readResultsFromFolder(folder, comparison, strategies, num_robots, nodes, alpha, rho, trials, bias):
    experiments_dict = utils.createExperimentsDict()
    for subdir, dirs, files in os.walk(folder):
        files.sort()
        for file_name in files:
            experiment_parameters = utils.getFileParameters(file_name)
            if not selectExperiment(experiment_parameters, comparison, strategies, num_robots, nodes, alpha, rho, trials, bias):
                continue
            
            print(file_name)
            experiments_dict = io_scripts.openTSVtoList(experiments_dict, folder, file_name, experiment_parameters)

    df = pd.DataFrame(experiments_dict)
    print(df)
    return df

def showStrategiesEvaluation():
    ### data_folder or arena_sizes_folder ###
    folder = data_folder 
    ### "Many Comparision", "Bias Comparision", "Encoded", "Arena Sizes" ###
    experiment_type = "Many Comparision"
    bias = True
    strategies = ["crwlevy", "rbn"]
    nodes = [18,20,22,24,26,28,30]
    alpha = 1.8
    rho = 0.75
    trials = 100
    robots = [20]

    data = readResultsFromFolder(folder, experiment_type, strategies, robots, nodes, alpha, rho, trials, bias)
    # data_2 = readResultsFromFolder(folder, experiment_type, ["ebn"], robots, nodes, alpha, rho, trials, bias)
    # # print(df2)
    # data = data.append(data_2)
    # print(data)
    # data = data.loc[(data["Name"] != "20N-7")]

    # data2 = utils.selectBestResultsFromDF(data)
    # data = utils.setComparisonWith(data2, "LMCRW", )
    # print(data)

    ### "Name", "Strategy", "Arena Size" ###
    x_values = "Name"
    ### "First Passage Time", "Fraction Discovery" ###
    y_values = "First Passage Time"
    ### "Name", "Strategy", "Bias", "Encode", "id" ###
    hue_values = "Strategy"

    # print(data.groupby(data.Name == "RBN 20N").mean())
    # print(data.mean())
    # if y_values == "Fraction Discovery":
    # data = utils.transformTimeSimulationToPerc(data)
    plot_func.boxplotComparison(data, robots, x_values, y_values, hue_values)
    # plot_func.boxplotMultiPlot(data, robots, x_values, y_values, hue_values)
    # plot_func.boxplotPairComparison(data, robots, x_values, y_values, hue_values)
    # plot_func.boxplotGroupedComparasion(data, robots, x_values, y_values, hue_values)
    # plot_func.violinPlotShowAllDistributionsComparison(data)
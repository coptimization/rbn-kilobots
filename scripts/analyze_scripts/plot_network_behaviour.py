import scripts.io_scripts as io_scripts
import scripts.plot_functions as plot_functions

behaviour_file_path = "Data/Behaviour Analysis/Behaviour results/"

def main(bn_type):
    sim_time = 3000
    max_steps = 10000
    num_nodes = [18,20,22,24,26,28,30]
    g_evaluations = 75
    networks = 100

    all_networks = dict()
    io_scripts.readChaoticBehaviourFile(behaviour_file_path, all_networks, networks, g_evaluations, max_steps)
    io_scripts.readChaoticBehaviourFile(behaviour_file_path, all_networks, 6, g_evaluations, max_steps)
    print(all_networks.keys())

    for plot_name in ["delta,sl"]:
        plot_functions.plotPerformanceOverBehaviourComparison(all_networks, num_nodes, bn_type, plot_name, True)
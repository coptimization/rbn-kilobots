import os
import csv
import numpy as np
import pandas as pd
from scipy import stats
import math
import matplotlib.pyplot as plt
import seaborn as sns
import scripts.utils as utils
import scripts.io_scripts as io_scripts
import scripts.plot_functions as plot_func

data_folder = "Data/Weibull Discovery time data/default arena/"

def testOfNormality(df):
    df['Normal Dist.'] = np.nan
    strategies = df['Name'].unique()
    for strategy in strategies:
        fpt_values = df.loc[df['Name'] == strategy]["First Passage Time"].values
        _, p = stats.shapiro(fpt_values)
        is_normal = 1 if p > 0.05 else 0
        # print("%s %s (p=%f)" % (strategy, "Normal distribution" if is_normal else "Nonparametric distribution", p))
        df['Normal Dist.'] = np.where(df['Name'] == strategy, is_normal, df['Normal Dist.'])

    return df

def mannWhitneyUtest(results1, results2, bonferroni):
    U1, p = stats.mannwhitneyu(results1, results2)
    nx, ny = len(results1), len(results2)
    U2 = nx*ny - U1
    # print(" %s U1=%d - %s U2=%d ---> p=%f" % (key1,U1,key2,U2,p))
    # print(stats.mannwhitneyu(results1, results2))
    conclusion = "is different" if p<bonferroni else "can't prove it's not different"

    return conclusion, p

def calculate_tTest(results1, results2, bonferroni):
    # print(stats.ttest_ind(results1, results2))
    stat, p = stats.ttest_ind(results1, results2)
    conclusion = "is different" if p<bonferroni else "can't prove it's not different"

    return conclusion, p

def multipleComparisonSignificanceTest(df, strategy_name, n_samples):
    significance = 0.05
    bonferroni = significance/n_samples
    strategies = df['Name'].unique().tolist()
    strategies.remove(strategy_name)
    best_rbns = utils.getResultsNameBetterThan(df, 'LMCRW')
    
    compare_networks = dict()
    print("Statistical Significance Test for %s (p=%.2f, Bonferoni Correction is p=%f):"% (strategy_name, significance, bonferroni))
    fpt_values_1 = df.loc[df['Name'] == strategy_name]["First Passage Time"].values
    is_normal_1 = df.loc[df['Name'] == strategy_name]["Normal Dist."].values[0]
    fpt_values_1.sort()
    for strategy_2 in strategies:
        fpt_values_2 = df.loc[df['Name'] == strategy_2]["First Passage Time"].values
        fpt_values_2.sort()
        is_normal_2 = df.loc[df['Name'] == strategy_2]["Normal Dist."].values[0]
        if is_normal_1 and is_normal_2:
            conclusion, p = calculate_tTest(fpt_values_1, fpt_values_2, bonferroni)
            # print("%s - %s: %s (T-test - p=%f)" % (strategy_name, strategy_2, conclusion, p))
        else:
            conclusion, p = mannWhitneyUtest(fpt_values_1, fpt_values_2, bonferroni)
            # print("%s - %s: %s (MannWhitney U test - p=%f)" % (strategy_name, strategy_2, conclusion, p))

        if conclusion != "is different":
            compare_networks[strategy_2] = 'Similar'
        elif strategy_2 in best_rbns:
            compare_networks[strategy_2] = 'Better'
        else:
            compare_networks[strategy_2] = 'Worse'

    equal_num = sum([1 for result in compare_networks.keys() if compare_networks[result] == 'Similar'])
    worst_num = sum([1 for result in compare_networks.keys() if compare_networks[result] == 'Worse'])
    best_num = sum([1 for result in compare_networks.keys() if compare_networks[result] == 'Better'])
    print(f"Worst networks: {worst_num} of {len(strategies)} = {(worst_num/len(strategies)*1.0)*100:.2f}%")
    print(f"Equal networks: {equal_num} of {len(strategies)} = {(equal_num/len(strategies)*1.0)*100:.2f}%")
    print(f"Best networks: {best_num} of {len(strategies)} = {(best_num/len(strategies)*1.0)*100:.2f}%\n")

    df = utils.setComparisonWith(df, "LMCRW", compare_networks)
    return df

def multipleCrossComparisonSignificanceTest(df):
    significance = 0.05
    strategies = df['Name'].unique()
    num_tests = math.factorial(len(strategies))/(math.factorial(2)*math.factorial(len(strategies)-2))
    bonferroni = significance/num_tests

    print("Statistical Significance Test (for p=0.05, Bonferoni Correction is p=%f):"% (bonferroni))
    for idx_1, strategy_1 in enumerate(strategies):
        fpt_values_1 = df.loc[df['Name'] == strategy_1]["First Passage Time"].values
        is_normal_1 = df.loc[df['Name'] == strategy_1]["Normal Dist."].values[0]
        fpt_values_1.sort()
        # print("Average: %d - Median: %d - Best Results: %d" % (np.mean(fpt_values_1),np.median(fpt_values_1), min(fpt_values_1)))
        for idx_2, strategy_2 in enumerate(strategies[idx_1+1:]):
            print("%s - %s:" % (strategy_1,strategy_2), end = ' ')
            fpt_values_2 = df.loc[df['Name'] == strategy_2]["First Passage Time"].values
            fpt_values_2.sort()
            is_normal_2 = df.loc[df['Name'] == strategy_2]["Normal Dist."].values[0]
            # print(f'Variance: {np.var(fpt_values_1)}, {np.var(fpt_values_2)}')
            if is_normal_1 and is_normal_2:
                # if sample_size > 30:
                conclusion, p = calculate_tTest(fpt_values_1, fpt_values_2, bonferroni)
                print("%s (T-test - p=%f)" % (conclusion,p))
                # else:
                #     conclusion, p = calculate_zTest(fpt_values_1, fpt_values_2, bonferroni)
                #     print("%s (Z-test - p=%f)" % (conclusion,p))
            else:
                conclusion, p = mannWhitneyUtest(fpt_values_1, fpt_values_2, bonferroni)
                print("%s (MannWhitney U test - p=%f)" % (conclusion,p))

            # utils.violinPlot(fpt_values_1, fpt_values_2, strategy_1, strategy_2, conclusion, p, bonferroni)
            # utils.plotDistribution(results1, results2, key1, key2, conclusion, p, bonferroni)

### ------------------------------------------------------------------------------------------------------------------------------- ###

def main():
    strategies = ["crwlevy", "ebn"]
    nodes = [18,20,22,24,26,28,30]
    alpha = 1.8
    rho = 0.75
    trials = 100
    num_robots = 20
    n_distributions = 100

    data_bns = pd.DataFrame()
    data_bns = io_scripts.readResultsFromFolder(data_folder, strategies, num_robots, [], alpha, rho, trials)
    for node in nodes:
        data_rbn = io_scripts.readResultsFromFolder(data_folder, ["crwlevy", "rbn"], num_robots, [node], alpha, rho, trials)
        # num_random_samples = 15
        # for n_random in range(0, num_random_samples):
        #     # data_aux = utils.selectNRandomResults(data_rbn.copy(), n_samples=n_distributions)
        #     # data_aux = utils.selectNfromBestResults(data_rbn, n_samples=n_distributions)
        #     data_aux["Name"] = data_aux["Name"].apply(lambda x: x.split("-")[0])
        #     data_aux["Name"] = data_aux["Name"].apply(lambda x: f"{x.replace('RBN ', '')}-{n_random+1}" if "RBN" in x else x)
        data_rbn = testOfNormality(data_rbn)
        data_rbn = multipleComparisonSignificanceTest(data_rbn, 'LMCRW', n_distributions)
        # data_rbn = multipleCrossComparisonSignificanceTest(data_rbn)
        data_bns = pd.concat([data_bns, data_rbn[20:]], axis=0, ignore_index=True)

        # data_ebn = io_scripts.readResultsFromFolder(data_folder, ["crwlevy", "ebn"], num_robots, [node], alpha, rho, trials)
        # # print(data_ebn)
        # # data = utils.selectBestResultsFromDF(data)
        # # data_ebn = utils.selectNfromBestResults(data_ebn, n_samples=n_distributions)
        # data_ebn = testOfNormality(data_ebn)
        # data_ebn = multipleComparisonSignificanceTest(data_ebn, 'LMCRW', n_distributions)
        # # data_ebn = multipleCrossComparisonSignificanceTest(data_ebn)
        # data_ebn["Name"] = data_ebn["Name"].apply(lambda x: x.split("-")[0].replace('EBN ', ''))
        # data_bns = pd.concat([data_bns, data_ebn[20:]], axis=0, ignore_index=True)

    data_bns['Comparison'] = np.where(data_bns['Strategy'] == "LMCRW", "LMCRW", data_bns["Comparison"])
    print(data_bns)
    # print(data_bns.to_string())

    # data_ebn = io_scripts.readResultsFromFolder(data_folder, strategies, num_robots, nodes, alpha, rho, trials)
    # # data_ebn = data_ebn.loc[(data_ebn["Name"] != "20N-7")]
    # print(data_ebn)
    # data_ebn = testOfNormality(data_ebn)
    # data_ebn = multipleComparisonSignificanceTest(data_ebn, 'LMCRW', 100)
    # # new_df = pd.concat([data_ebn, data_bns], axis=0)
    # print(data_ebn)
    
    # barplot_df = utils.createStrategyComparisonDF(new_df[20:])
    # plot_func.barplotGroupedComparison(barplot_df, 20)

    # new_df['Comparison'] = np.where(new_df['Strategy'] == "EBN 22N", "EBN 22N", new_df["Comparison"])
    # new_df['Comparison'] = np.where(new_df['Strategy'] == "EBN 30N", "EBN 30N", new_df["Comparison"])
    # plot_func.boxplotComparison(data_bns, num_robots, "Name", "First Passage Time", "Strategy")
    # plot_func.violinPlotShowAllDistributionsComparison(new_df)
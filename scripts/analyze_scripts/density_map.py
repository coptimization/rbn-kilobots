import utils
import numpy as np
import os
import pandas as pd

dir_folder = "../Data/EBN position data/EBN_encoded_robots#30_nodes#50"
results_dir = "../results/plots/density_maps/"

def density_map_generation(folder_experiments, b_edges, distance_heatmap_dir):
    for dirName, subdirList, fileList in os.walk(folder_experiments):
        # print(dirName)
        num_robots = "-1"
        num_nodes = "-1"
        encoded = ""
        elements = dirName.split("_")
        for e in elements:
            if (e.startswith("encoded")):
                encoded = "encoded"
            if e.startswith("robots"):
                num_robots = e.split("#")[-1]
            if (e.startswith("nodes")):
                num_nodes = e.split("#")[-1]

        if (num_robots == "-1" or num_nodes == "-1"):
            continue

        # print("dirName: ", dirName)
        runs = len([f for f in fileList if
                    (os.path.isfile(os.path.join(dirName, f)) and f.endswith('position.tsv'))])
        # print("runs: ", runs)

        number_of_experiments = 0
        df_experiment = pd.DataFrame()

        [number_of_experiments, df_experiment] = utils.load_pd_positions(dirName, "experiment")

        #     print(number_of_experiments)
        positions_concatenated = df_experiment.values[:, 1:1000]
        [num_robot, num_times] = positions_concatenated.shape
        positions_concatenated = np.array([x.split(',') for x in positions_concatenated.ravel()], dtype=float)
        positions_concatenated = positions_concatenated.reshape(num_robot, num_times, 2)
        #         print(positions_concatenated.shape)

        distances = utils.distance_from_the_origin(positions_concatenated)
        occurrences = utils.get_occurrences(distances, b_edges, runs)

        utils.time_plot_histogram(occurrences.T, b_edges[1:], num_nodes, encoded, num_robots,
                                distance_heatmap_dir)


density_map_generation(dir_folder, np.linspace(0, 0.45, 20), results_dir)

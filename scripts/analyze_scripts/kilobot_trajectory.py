import pandas as pd
import os
import time
import matplotlib.pyplot as plt
import numpy as np
from colour import Color
from scipy.interpolate import griddata
import seaborn as sns
import math
from matplotlib.collections import EllipseCollection
from copy import copy
import scripts.plot_functions as plot_functions
import scripts.utils as utils
import src.utils.Targets as Targets

arena_radius = 0.475
kilobot_comm_radius = 0.02
target_file = "rbn_performance_targets.txt"
ebn_positions_folder = "Data/Kilobot_position_data/Positions tsv/"

def getRobotsPositions(dir_path, pck_filename):
    num_trials, df = utils.loadPositionsFile(dir_path, pck_filename)
    positions_concatenated = df.values[:, 1:]
    [num_trials_and_robots, num_times] = positions_concatenated.shape
    num_robots = int(num_trials_and_robots/num_trials)
    positions_concatenated = np.array([x.split(',') for x in positions_concatenated.ravel()], dtype=float)
    if num_robots == 1:
        positions_concatenated = positions_concatenated.reshape(num_trials, num_times, 2)
    else:
        positions_concatenated = positions_concatenated.reshape(num_trials, num_robots, num_times, 2)

    # print(positions_concatenated.shape)
    # print(positions_concatenated[0])
    return positions_concatenated

def readKilobotPositions(strategies, alpha_values, rho_values, num_nodes, arena, num_robots, kilobot_bias, sample_period):
    positions_data = dict()
    for subdir, dirs, files in os.walk(ebn_positions_folder):
        dirs.sort()
        for sim_folder in dirs:
            elements = sim_folder.split("_")
            file_strategy = elements[0]
            file_robots = -1
            file_node = -1
            file_alpha = -1
            file_rho = -1
            file_seed = -1
            file_bias = ""
            file_arena = 90
            file_samplep = -1
            for e in elements[1:]:
                if e.startswith("robots"):
                    file_robots = int(e.split("#")[1])
                if e.startswith("nodes"):
                    file_node = int(e.split("#")[1])
                if e.startswith("alpha"):
                    file_alpha = float(e.split("#")[1])
                if e.startswith("rho"):
                    file_rho = float(e.split("#")[1])
                if e.startswith("bias"):
                    file_bias = e.split("#")[1]
                if e.startswith("arena"):
                    arena_float = float(e.split("#")[1]) - 0.025
                    file_arena = int(arena_float*100)*2
                if e.startswith("samplep"):
                    file_samplep = int(e.split("#")[1])
                if e.startswith("id"):
                    file_seed = int(e.split("#")[1])

            if file_robots != num_robots or file_bias != kilobot_bias or file_samplep != sample_period or file_arena != arena:
                continue

            if file_strategy not in strategies:
                continue
            else:
                if file_strategy == "CRWLEVY":
                    if file_alpha not in alpha_values or file_rho not in rho_values:
                        continue
                elif file_node not in num_nodes:
                    continue

            if file_seed == -1:
                continue

            if file_seed > 5:
                continue

            print(f"Loading trials for: {sim_folder}")
            # positions_data[sim_folder] = []
            # for trial_file in os.listdir(ebn_positions_folder + sim_folder):
            # positions_file = ebn_positions_folder + sim_folder + "/" + trial_file
            # positions = getRobotsPositions(positions_file)
            # positions_data[sim_folder].append(positions)
            positions = getRobotsPositions(ebn_positions_folder + sim_folder, sim_folder)
            positions_data[sim_folder] = positions
            
    return positions_data

def showRobotTrajectory(data, show_target, num_trials, num_robots, arena_radius):
    targets_pos = []
    if show_target:
        targets_pos = [(0.38,0), (0,0.38), (-0.38,0), (0,-0.38), (0.25,0.0), (0.0,0.25), (-0.25, 0.0), (0.0,-0.25)]
        # targets_pos = [(0.38,0), (0,0.38), (-0.38,0), (0,-0.38), (0.141,0.141), (-0.141,0.141), (0.141,-0.141), (-0.141,-0.141)]
        # targets_pos = Targets.readTargetPosition()

    trials = np.arange(0, num_trials)
    for experiment in data:
        if num_robots > 1:
            for trial in trials:
                title = f"{experiment} - trial:{trial}"
                plot_functions.plotMultiTrajectory(data[experiment][trial], title, targets_pos, arena_radius)
        else:
            if len(trials) > 1:
                title = f"{experiment} - trials:{trials[0]+1} to {trials[-1]+1}"
                data_trials = data[experiment][trials[0]:len(trials)]
                plot_functions.plotMultiTrajectory(data_trials, title, targets_pos, arena_radius)
            else:
                title = f"{experiment} - trial:{trials[0]}"
                plot_functions.plotTrajectory(data[experiment], title, targets_pos, arena_radius)

def loadAllGrids(data_positions):
    all_grids = []
    num_grids = data_positions.shape[0]
    for n in range(num_grids):
        positions = data_positions[n, :]
        pos_x, pos_y = zip(*positions)
        grid = utils.createArenaGrid(kilobot_comm_radius)
        grid = utils.addPositionsToArenaGrid(grid, pos_x, pos_y, kilobot_comm_radius)
        all_grids.append(grid)

    return all_grids

def showKilobotGridPresence(data, targets_pos, trials, num_robots):
    for experiment in data:
        if num_robots > 1:
            for trial in trials:
                title = f"{experiment} - trial:{trial}"
                all_grids = loadAllGrids(data[experiment][trial])
                plot_functions.plotMultiGrid(all_grids, title, targets_pos)
        else:
            if len(trials) > 1:
                title = f"{experiment} - trials:{trials[0]+1} to {trials[-1]+1}"
                all_grids = loadAllGrids(data[experiment][trials[0]:len(trials)])
                plot_functions.plotMultiGrid(all_grids, title, targets_pos)

            else:
                title = f"{experiment} - trial:{trials[0]}"
                all_grids, = loadAllGrids(data[experiment])
                plot_functions.plotSingleGrid(all_grids, title, targets_pos)

def showSwarmGridPresenceByTime(data, targets_pos, trials, num_robots, timestamp):
    for experiment in data:
        if num_robots > 1:
            for trial in trials:
                title = f"{experiment} - trial:{trial}"
                print(f"Creating Grid for {title}")
                data_positions = data[experiment][trial]
                all_grids = []
                grid_titles = []
                total_time = data_positions.shape[1]
                cumulative_grid = utils.createArenaGrid(kilobot_comm_radius)
                for sim_time in range(0, total_time-1, timestamp):
                    new_grid = utils.createArenaGrid(kilobot_comm_radius)
                    for robots in range(num_robots):
                        positions = data_positions[robots, sim_time:sim_time+timestamp]
                        pos_x, pos_y = zip(*positions)
                        new_grid = utils.addPositionsToArenaGrid(cumulative_grid, pos_x, pos_y, kilobot_comm_radius)
                    all_grids.append(new_grid)
                    grid_titles.append(f"Timestamp:{sim_time+timestamp}")
                    cumulative_grid = new_grid.copy()

                if num_robots > 1 or len(trials) > 1:
                    plot_functions.plotMultiGrid(all_grids, title, targets_pos, suptitles=grid_titles)
                else:
                    plot_functions.plotSingleGrid(all_grids, title, targets_pos)
        else:
            print("Only 1 robot!")
            return

# def showKilobotsHeatMap(df, title, num_robots):
#     index = np.arange(-0.475,0.476,0.005, dtype=float)
#     index_str = ['{:.3f}'.format(x) for x in index]
#     d = dict()
#     for i in index_str:
#         d[i] = np.zeros(len(index), dtype=int)

#     grid = pd.DataFrame(data=d, index=index_str)
#     deleteValuesOutOfArena(grid, index)
#     for robot in range(df.shape[0]):
#         positions = df[robot, :]
#         pos_x, pos_y = zip(*positions)
#         for i in range(len(pos_x)):
#             col = '{:.3f}'.format(min(index, key=lambda x:abs(x-pos_y[i])))
#             idx = '{:.3f}'.format(min(index, key=lambda x:abs(x-pos_x[i])))
#             # print("%f %f -> %s %s" % (pos_x[i], pos_y[i], col, idx))
#             grid.loc[col, idx] += 1

#         # max_value = grid.max(numeric_only=True).max()
#         # base = Color("green")
#         # colors = list(base.range_to(Color("red"), max_value))
#         # ax.scatter(pos_x, pos_y, s=10, marker="s")
#         # plt.title("Kilobot trajectory for %s %dN" % ("EBN", 22))
#         # plt.tight_layout()
#         # plt.ylim((-0.475,0.475))
#         # plt.xlim((-0.475,0.475))
#         # plt.show()

#         # max_r = 0.475
#         # max_theta = 2.0 * np.pi
#         # number_points = len(positions)
#         # points = positions
#         # theta = np.linspace(0.0, max_theta, 100)
#         # r = np.linspace(0, max_r, 200)
#         # grid_r, grid_theta = np.meshgrid(r, theta)
#         # data = griddata(points, grid, (grid_r, grid_theta), method='cubic', fill_value=0)

#         # #Create a polar projection
#         # ax1 = plt.subplot(projection="polar")
#         # ax1.pcolormesh(theta,r,data.T)
#         # plt.show()

#     plt.title("Kilobot density for " + title)
#     ax = sns.heatmap(grid)
#     ax.invert_yaxis()
#     plt.show()

# def showKilobotsPresenceGrid(df, title, num_robots, step):
#     index = np.arange(-0.475,0.476,0.015, dtype=float)
#     index_str = ['{:.3f}'.format(x) for x in index]
#     d = dict()
#     for i in index_str:
#         d[i] = np.zeros(len(index), dtype=int)

#     grid = pd.DataFrame(data=d, index=index_str)
#     num_none_values = deleteValuesOutOfArena(grid, index)
#     print(df.shape)
#     fig, ax = plt.subplots(2, 5, figsize=(16, 9), sharey=True, sharex=True)
#     c = 0
#     for line in range(2):
#         for colunm in range(int(num_robots/2)):
#             for robot in range(df.shape[0]):
#                 positions = df[robot, step*c:step*(c+1)]
#                 pos_x, pos_y = zip(*positions)
#                 for i in range(len(pos_x)):
#                     col = '{:.3f}'.format(min(index, key=lambda x:abs(x-pos_y[i])))
#                     idx = '{:.3f}'.format(min(index, key=lambda x:abs(x-pos_x[i])))
#                     # print("%f %f -> %s %s" % (pos_x[i], pos_y[i], col, idx))
#                     grid.loc[col, idx] = 1

#             num_ones = grid[grid == 1].count().sum()
#             search_perc = (num_ones /((len(index)**2)-num_none_values)) * 100
#             sns.heatmap(grid, ax=ax[line][colunm], cbar=False, xticklabels=False, yticklabels=False)
#             ax[line][colunm].invert_yaxis()
#             ax[line][colunm].set_title("%d tickets - %d%%" % (step*(c+1), search_perc))
#             c += 1

#     fig.suptitle("Grid presence for " + title, fontsize=20)
#     plt.show()

# def plotPositionAttractor3D(df, title):
#     fig = plt.figure()
#     ax = fig.gca(projection='3d')
#     lag = 10

#     x = np.array([p[0] for p in df[0][:-(2*lag):lag]], dtype=float)
#     y = np.array([p[0] for p in df[0][lag:-lag:lag]], dtype=float)
#     z = np.array([p[0] for p in df[0][(2*lag)::lag]], dtype=float)

#     ax.plot(x, y, z, label='kilobot positional attractor')
#     ax.legend()  
#     ax.set_xlabel('x')
#     ax.set_ylabel('x+%d'%(lag))
#     ax.set_zlabel('x+%d'%(2*lag))
#     ax.set_title(title)  
#     fig.set_size_inches(16, 9)
#     plt.show()

# def deleteValuesOutOfArena(grid, index):
#     cont = 0
#     for i1, x in enumerate(index):
#         for i2, y in enumerate(index):
#             radius = math.sqrt(pow(x,2) + pow(y,2))
#             if radius > 0.475:
#                 grid.iloc[i1,i2] = None
#                 cont += 1
#     return cont

def trajectoryAnalisys():
    strategies = ["RBN"]
    num_nodes = [18, 20, 22, 24, 26, 30]
    alpha_values = [2.0]
    rho_values = [0.75]
    arena_radius = 180
    num_robots = 20
    trials = 1
    kilobot_bias = "original"
    sample_period = 10
    timestamp = int(3000/10)
    show_target = False

    positions_data = readKilobotPositions(strategies, alpha_values, rho_values, num_nodes, arena_radius, num_robots, kilobot_bias, sample_period)

    showRobotTrajectory(positions_data, show_target, trials, num_robots, arena_radius) 
    # showKilobotGridPresence(positions_data, target_pos, trials, num_robots)
    # showSwarmGridPresenceByTime(positions_data, target_pos, trials, num_robots, timestamp)
    # pgrid_steps = [50,300]
    # for s in pgrid_steps:
    #     showKilobotsPresenceGrid(robots_positions, plot_title, 10, s)
    # showKilobotsHeatMap(robots_positions, plot_title, num_robots)
    # plotPositionAttractor3D(robots_positions, plot_title)
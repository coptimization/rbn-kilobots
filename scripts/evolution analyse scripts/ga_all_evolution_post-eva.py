import ga_best_ebn_post_evaluation
import os
import time

n_nodes = 30
n_robots = 20
evolutions_folder = "../Data/Evolution Data/"
date_exception = "2021-12-22"

def getAllEvolutions(num_nodes, num_robots):
    dirs = []
    for dir in os.listdir(evolutions_folder):
        elements = dir.split("_")
        date = ""
        dir_nodes = -1
        dir_robots = -1
        for e in elements:
            if e.endswith("N"):
                dir_nodes = int(e.replace("N", ""))
            if e.endswith("R"):
                dir_robots = int(e.replace("R", ""))
            if e.startswith("202"):
                date = e
            if e == "nsga2":
                continue
            

        if num_nodes == dir_nodes and num_robots == dir_robots:
            if date != date_exception:
                dirs.append(dir)
    dirs.sort()
    ga_best_ebn_post_evaluation.evaluateMultipleEvolutions(dirs)

getAllEvolutions(n_nodes, n_robots)
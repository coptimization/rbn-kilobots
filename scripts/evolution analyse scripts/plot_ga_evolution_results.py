import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits import mplot3d
import csv
import time
from operator import itemgetter
from scipy.optimize import curve_fit
from scipy.ndimage.filters import uniform_filter1d
from scipy.signal import savgol_filter
import os
import statistics
import math

evolution_curve_data = "../Data/Evolution Data/2022-04-22_ga_20R_30N_fixed_targets/evolution_curve_data.txt"
post_evaluation_gen_data = "../results/Post evolution evaluation/GA Best EBN from Gen/2022-04-22_posteva_best_per_genenartion_20R_30N.txt"

num_nodes = 30
num_postevo_trials = 100
num_evo_trials = 8
num_robots = 20

def plotEvolutionCurve():
    disc_weibull = []
    disc_time = []
    frac_disc = []
    inf_time = []
    frac_inf = []
    with open(evolution_curve_data, 'r') as data:
        try:
            lines = data.readlines()
            for line in lines:
                disc_weibull.append(int(line.split()[0]))
                disc_time.append(int(line.split()[1]))
                frac_disc.append(float(line.split()[2]))
                inf_time.append(int(line.split()[3]))
                frac_inf.append(float(line.split()[4]))
            data.close()
        except Exception as e:
            print("Couldnt open data file!\n" + str(e))

    xdata = np.arange(len(disc_weibull))

    popt, pcov = curve_fit(linearFunc, xdata, disc_weibull)
    plt.title("GA Evolution Curve of the Mean Weibull Discovery Time (%dN, %d robots)" % (num_nodes, num_robots))
    plt.xlabel("Generation")
    plt.ylabel("Weibull Discovery time")
    plt.plot(disc_weibull, '.', color="green")
    plt.plot(xdata, linearFunc(xdata, *popt))
    plt.grid(b=True, which='major', color='#666666', linestyle='-', alpha=0.5)
    plt.minorticks_on()
    plt.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.2)
    plt.show()

    popt, pcov = curve_fit(linearFunc, xdata, frac_disc)
    plt.title("GA Evolution Curve of Fraction Discovery (%dN, %d robots)" % (num_nodes, num_robots))
    plt.xlabel("Generation")
    plt.ylabel("Fraction Discovery")
    plt.grid(b=True, which='major', color='#666666', linestyle='-', alpha=0.5)
    plt.minorticks_on()
    plt.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.2)
    plt.plot(frac_disc, '.', color="green")
    plt.plot(xdata, linearFunc(xdata, *popt))
    plt.show()

    popt, pcov = curve_fit(linearFunc, xdata, inf_time)
    plt.title("GA Evolution Curve of Total Information Time (%dN, %d robots)" % (num_nodes, num_robots))
    plt.xlabel("Generation")
    plt.ylabel("Information Time")
    plt.grid(b=True, which='major', color='#666666', linestyle='-', alpha=0.5)
    plt.minorticks_on()
    plt.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.2)
    plt.plot(inf_time, '.', color="green")
    plt.plot(xdata, linearFunc(xdata, *popt))
    plt.show()

    weight_curve_mean = uniform_filter1d(disc_weibull, size=100)
    plt.title("GA Evolution Curve of Weibull Discovery Time (%dN, %d robots)" % (num_nodes, num_robots))
    plt.xlabel("Generation")
    plt.ylabel("Weibull Discovery Time")
    plt.grid(b=True, which='major', color='#666666', linestyle='-', alpha=0.5)
    plt.minorticks_on()
    plt.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.2)
    plt.plot(weight_curve_mean)
    plt.show()

def plotEvolutionCurveComparision():
    main_dir = "Data/Evolution Data/Evolutions"
    all_dirs = os.listdir(main_dir)
    all_dirs.sort()
    for evolution_dir in all_dirs:
        num_robots = "-1"
        num_nodes = "-1"
        date = ""
        algorithm_name = ""
        elements = evolution_dir.split("_")
        algorithm_name = elements[1]
        for e in elements:
            if (e.startswith("202")):
                date = e
            if e.endswith("R"):
                num_robots = e[:-1]
            if (e.endswith("N")):
                num_nodes = e[:-1]

        if (num_robots == "-1" or num_nodes == "-1" or algorithm_name != "ga"):
            continue

        # Check specific number of node and robots
        if (num_robots != "20"):
            continue

        if (num_nodes != "30"):
            continue

        # Check date of experiments
        if ((not date.startswith("2022"))):
            continue
        print(elements)

        disc_weibull = []
        disc_time = []
        frac_disc = []
        inf_time = []
        frac_inf = []
        evolution_curve_path_txt = main_dir + "/" + evolution_dir + "/evolution_curve_data.txt"
        if os.path.exists(evolution_curve_path_txt):
            with open(evolution_curve_path_txt, 'r') as data:
                try:
                    lines = data.readlines()
                    for line in lines:
                        disc_weibull.append(int(line.split()[0])/32.0)
                        disc_time.append(int(line.split()[1]))
                        frac_disc.append(float(line.split()[2]))
                        inf_time.append(int(line.split()[3]))
                        frac_inf.append(float(line.split()[4]))
                    data.close()
                except Exception as e:
                    print("Couldnt open data file!\n" + str(e))
        else:
            with open(main_dir + "/" + evolution_dir + "/evolution_curve_data.tsv", 'r') as data:
                try:
                    read_tsv = csv.reader(data, delimiter="\t")
                    next(read_tsv, None)
                    for row in read_tsv:
                        print(float(row[1])/32.0)
                        disc_weibull.append(float(row[1])/32.0)
                        disc_time.append(float(row[2])/32.0)
                        frac_disc.append(float(row[3]))
                        inf_time.append(float(row[4])/32.0)
                        frac_inf.append(float(row[5]))
                    data.close()
                except Exception as e:
                    print("Couldnt open data file!\n" + str(e))

        date = evolution_dir.split("_")[0]
        # weight_curve_mean = uniform_filter1d(disc_weibull, size=10)
        # weight_curve_mean = savgol_filter(disc_weibull, 10)
        # plt.plot(weight_curve_mean, label="%s"%(date))
        plt.plot(disc_weibull, label="%s"%(date))

    plt.title("GA Evolution Curve of Weibull Discovery Time (%sR - %sN)" % (num_robots, num_nodes))
    plt.xlabel("Generation")
    plt.ylabel("Weibull Discovery Time")
    plt.grid(b=True, which='major', color='#666666', linestyle='-', alpha=0.5)
    plt.minorticks_on()
    plt.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.2)
    plt.legend()
    plt.show()

def linearFunc(x, a, b):
    return (a*x) + b

def plotEvolutionEvaluationPerformance():
    gen = []
    posteva_disc_weibull = []
    with open(post_evaluation_gen_data , 'r') as data:
        try:
            lines = data.readlines()
            for line in lines:
                posteva_disc_weibull.append(int(line.split()[1]))
                gen.append(int(line.split()[-1])-1)
            data.close()
        except Exception as e:
            print("Couldnt open data file!\n" + str(e))

    disc_weibull = []
    raw_disc_weibull = []
    with open(evolution_curve_data, 'r') as data:
        try:
            lines = data.readlines()
            count = 0
            for n, line in enumerate(lines):
                raw_disc_weibull.append(int(line.split()[0]))
                if count < len(gen):
                    if n == (gen[count]):
                        disc_weibull.append(int(line.split()[0]))
                        count += 1
            data.close()
        except Exception as e:
            print("Couldnt open data file!\n" + str(e))

    date = post_evaluation_gen_data.split("/")[-1].split("_")[0]
    plt.title("Comparison between GA and post-evaluation for Weibull Discovery Time (%dN, %d robots) - %s" % (num_nodes, num_robots, date))
    plt.xlabel("Generation")
    plt.ylabel("Weibull Discovery Time")
    plt.grid(b=True, which='major', color='#666666', linestyle='-', alpha=0.5)
    plt.minorticks_on()
    plt.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.2)
    # plt.plot(raw_disc_weibull, label="raw GA 8 trials", c="#53834a")
    plt.plot(gen, disc_weibull, label="GA 8 trials")
    plt.scatter(gen, disc_weibull, c="#04165c")
    plt.plot(gen, posteva_disc_weibull, label="Post-evaluation 100 trials")
    plt.scatter(gen, posteva_disc_weibull, c="#996413")
    plt.legend(loc="best")
    plt.show()

def plotPostEvaluationDifference():
    all_diff = []
    conf_values = []
    labels = []
    main_dir = "../Data/Evolution Data"
    for evolution_dir in os.listdir(main_dir):
        num_robots = "-1"
        num_nodes = "-1"
        date = ""
        algorithm_name = ""
        elements = evolution_dir.split("_")
        algorithm_name = elements[1]
        for e in elements:
            if (e.startswith("202")):
                date = e
            if e.endswith("R"):
                num_robots = e[:-1]
            if (e.endswith("N")):
                num_nodes = e[:-1]

        if (num_robots == "-1" or num_nodes == "-1" or algorithm_name != "ga"):
            continue

        # Check specific number of node and robots
        if (num_nodes != "30" or num_robots != "20"):
            continue

        # Check date of experiments
        # if ((not date.startswith("2021-12-22")) and (not date.startswith("2022-04-11"))):
        #     continue
        # print(elements)

        gen = []
        posteva_disc_weibull = []
        with open("../results/Post evolution evaluation/GA Best EBN from Gen/%s_posteva_best_per_genenartion_%sR_%sN.txt" % (date, num_robots, num_nodes), 'r') as data:
            try:
                lines = data.readlines()
                for line in lines:
                    posteva_disc_weibull.append(int(line.split()[1]))
                    gen.append(int(line.split()[-1]))
                data.close()
            except Exception as e:
                print("Couldnt open data file!\n" + str(e))

        disc_weibull = []
        raw_disc_weibull = []
        with open(main_dir + "/" + evolution_dir + "/evolution_curve_data.txt", 'r') as data:
            try:
                lines = data.readlines()
                count = 0
                for n, line in enumerate(lines):
                    raw_disc_weibull.append(int(line.split()[0]))
                    if count < len(gen):
                        if (n+1) == (gen[count]):
                            disc_weibull.append(int(line.split()[0]))
                            count += 1
                            print(n)
                data.close()
            except Exception as e:
                print("Couldnt open data file!\n" + str(e))

        print(gen)
        print(posteva_disc_weibull)
        print(disc_weibull)

        sdv_posteva = statistics.stdev(posteva_disc_weibull)
        print(sdv_posteva)

        avg_ga = (sum(disc_weibull) / len(disc_weibull))
        z_95 = 1.96
        z_90 = 1.645
        avg_conf = int(avg_ga - (z_95 * (sdv_posteva/math.sqrt(len(disc_weibull)))))
        print(avg_conf)

        evaluation_dif = []
        for idx in range(len(disc_weibull)):
            evaluation_dif.append((abs(disc_weibull[idx] - posteva_disc_weibull[idx])/posteva_disc_weibull[idx])*100)
        # for idx in range(len(disc_weibull)):
        #     evaluation_dif.append((disc_weibull[idx]/posteva_disc_weibull[idx])*100)

        # plt.plot(gen, evaluation_dif, label=date)
        # plt.scatter(gen, evaluation_dif)

        average_dif = sum(evaluation_dif)/len(evaluation_dif)
        labels.append(date)
        all_diff.append(average_dif)
        conf_values.append(u"\u00B1" + str(avg_conf))

    num_nodes = "30"
    ### Difference for each evolution
    # plt.title("Difference between GA and Post-evaluation for Weibull Discovery Time (%sN, %s robots)" % (num_nodes, num_robots))
    # plt.xlabel("Generation")
    # plt.ylabel("Absolute Difference")
    # plt.grid(b=True, which='major', color='#666666', linestyle='-', alpha=0.5)
    # plt.minorticks_on()
    # plt.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.2)
    # plt.legend(loc="best")
    # # plt.ylim((0,max(evaluation_dif)+50))
    # plt.show()

    # ### Difference for all evolution
    # column_average = [sum(sub_list) / len(sub_list) for sub_list in zip(*all_diff)]
    # plt.plot(gen, evaluation_dif, label="%s"%(num_nodes))
    # plt.scatter(gen, evaluation_dif)
    # plt.title("Difference between GA and Post-evaluation for Weibull Discovery Time (%sN, %s robots)" % (num_nodes, num_robots))
    # plt.xlabel("Generation")
    # plt.ylabel("Absolute Difference")
    # plt.grid(b=True, which='major', color='#666666', linestyle='-', alpha=0.5)
    # plt.minorticks_on()
    # plt.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.2)
    # plt.legend(loc="best")
    # plt.show()

    ### Difference for each evolution
    Ncolors = len(all_diff)
    colormap = plt.cm.viridis  # LinearSegmentedColormap
    Ncolors = min(colormap.N, Ncolors)
    mapcolors = [colormap(int(x * colormap.N / Ncolors)) for x in range(Ncolors)]
    bar = plt.bar(np.arange(0,len(all_diff)), all_diff, color=mapcolors)
    for c, rect in enumerate(bar):
        height = rect.get_height() 
        plt.text(rect.get_x() + rect.get_width() / 2.0, height, "%.2f"% height, ha='center', va='bottom')
    plt.title("Difference between GA and Post-evaluation for Weibull Discovery Time (%sN, %s robots)" % (num_nodes, num_robots))
    plt.xlabel("Runs")
    plt.ylabel("(GA Trials / Posteva) (%)")
    # plt.ylabel("GA Trial/Post-evaluation (%)")
    plt.xticks(np.arange(0,len(all_diff)), labels)
    plt.grid(b=True, which='major', color='#666666', linestyle='-', alpha=0.5)
    plt.minorticks_on()
    plt.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.2)
    plt.show()



def main():
    # plotEvolutionCurve()
    plotEvolutionCurveComparision()
    # plotEvolutionEvaluationPerformance()
    # plotPostEvaluationDifference()

main()
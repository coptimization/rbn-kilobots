import sys
sys.path.append('../')
import rbn_performance_test
import random
import math

# Simulation parameters 
num_trials = 100
num_robots = 30
num_nodes = 50
arena_radius = 0.475

# Path to files
paretofront_file = "../Data/2021-07-27_nsga2_50N_fixed_targets/paretofront_individual_data.txt"
initial_state_file = "../Data/2021-07-27_nsga2_50N_fixed_targets/initial_state_file.txt"
new_paretofront_file = "../results/post_evaluation_paretofront_"

class RBN:
    def __init__(self, connections, bfunctions, rbn, nsga2_weibull, nsga2_inf):
        self.connections = connections
        self.bfunctions = bfunctions
        self.rbn_parameters = rbn
        self.nsga2_weibull_disc = nsga2_weibull
        self.nsga2_information_time = nsga2_inf

    def setResults(self, weibull_disc, disc, frac_disc, inf, frac_inf):
        self.weibull_disc = weibull_disc
        self.discovery_time = disc
        self.fraction_discovery = frac_disc
        self.information_time = inf
        self.fraction_information = frac_inf

def readParetoFront():
    paretofront = []
    connections = []
    bfunctions = []
    rbn = str(num_nodes)
    weibull = 0
    information = 0
    with open(paretofront_file, "r+") as file:
        try:
            lines = file.readlines()
            for line in lines:
                if line is "\n":
                    paretofront.append(RBN(connections, bfunctions, rbn, weibull, information))
                    rbn = str(num_nodes)
                    connections = []
                    bfunctions = []
                elif "fitness:" not in line:
                    rbn += line
                    if len(line) == (num_nodes + 1):
                        connections.append(list(map(int, line.strip('\n'))))
                    else:
                        bfunctions.append(list(map(int, line.strip('\n'))))
                elif "fitness:" in line:
                    weibull = int(line.split()[1])
                    information = int(line.split()[2])
            file.close()
        except Exception as e:
            print("Couldnt open paretofront file. Error: " + str(e))

    return paretofront

def readInitialStates():
    initial_states = []
    with open(initial_state_file, "r+") as init_states:
        try:
            for i in range(num_robots):
                initial_states.append([])
                line = init_states.readline()
                for ch in line:
                    if ch != '\n':
                        initial_states[i].append(int(ch))
            init_states.close()
        except Exception as e:
            print("Couldnt open initial states file. Error: " + str(e))
    return initial_states

def createTargetPosition(max_trials):
    targets_pos = []
    target_num = 0
    while(target_num < max_trials):
        theta = random.uniform(-math.pi, math.pi)
        rho = random.uniform(0, arena_radius - 0.025)
        pos_xy = (rho * math.cos(theta), rho * math.sin(theta))
        targets_pos.append(pos_xy)
        target_num += 1

    return targets_pos


def runSimulation(rbn, initial_states, targets_position):
    output = rbn_performance_test.readingRBNFromArguments(str(num_nodes), rbn.connections, rbn.bfunctions, initial_states, 
        targets_position, num_trials, False)
    if (output == None):
        sys.exit("Output has None value")
    return output

def saveParetofrontValues(rbn):
    with open(new_paretofront_file + str(num_nodes) + "N.txt", "a+") as file:
        try:
            file.write("fitness: %d %d %.2f %d %.2f %d %d\n" % (rbn.weibull_disc, rbn.discovery_time, rbn.fraction_discovery, rbn.information_time,
                rbn.fraction_information, rbn.nsga2_weibull_disc, rbn.nsga2_information_time))
            file.close()
        except Exception as e:
            print("Error saving new paretofront: " + str(e))

paretofront = readParetoFront()
initial_states = readInitialStates()

print("Starting post-evaluation process of Pareto Front EBNs.\nEBNs Number: %d\nNumber of trials: %d" % (len(paretofront), num_trials))
for count, rbn in enumerate(paretofront):
    print("Starting %d trials test for %d EBN." % (num_trials, (count + 1)))
    random.seed(10)
    targets_position = createTargetPosition(num_trials)
    weibull_disc, disc, frac_disc, inf, frac_inf = runSimulation(rbn, initial_states, targets_position)
    rbn.setResults(weibull_disc, disc, frac_disc, inf, frac_inf)
    saveParetofrontValues(rbn)
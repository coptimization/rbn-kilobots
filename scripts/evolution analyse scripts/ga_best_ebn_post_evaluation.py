import sys
sys.path.append('../')
import rbn_performance_test
import random
import math
import operator

# Evaluation parameters 
num_trials = 100
min_fitness = 30000
max_num_of_ebns = 15
gen_step = 100

# Simulation parameters 
arena_radius = 0.475

# Path to files
evolution_folder = "../Data/Evolution Data/"
posteva_best_ebns_file = "../results/posteva_ga_best_ebns_"
posteva_geneneration_ebn_file = "../results/posteva_best_per_genenartion_"

class EBN:
    def __init__(self, connections, bfunctions, ebn, ga_weibull, ga_inf):
        self.connections = connections
        self.bfunctions = bfunctions
        self.ebn_parameters = ebn
        self.ga_weibull_disc = ga_weibull
        self.ga_information_time = ga_inf

    def setResults(self, weibull_disc, disc, frac_disc, inf, frac_inf):
        self.weibull_disc = weibull_disc
        self.discovery_time = disc
        self.fraction_discovery = frac_disc
        self.information_time = inf
        self.fraction_information = frac_inf

    def setEBNInfo(self, n_nodes, n_robots, date):
        self.num_nodes = n_nodes
        self.num_robots = n_robots
        self.date = date

    def printEBNresultGA(self):
        print("%d " % self.ga_weibull_disc)

### ------------------------------------------------------------------------------------------- ###

def getEvolutionValues():
    num_nodes = -1
    num_robots = -1
    date = ""
    elements = evolution_folder.split("/")[-1].split("_")
    for e in elements:
        if e.endswith("N"):
            num_nodes = int(e.replace("N", ""))
        if e.endswith("R"):
            num_robots = int(e.replace("R", ""))
        if e.startswith("202"):
            date = e

    if num_nodes == -1 or num_robots == -1:
        print("Cant get number of nodes and/or robots")
        exit(1)

    return num_nodes, num_robots, date

def readBestEBNs():
    num_nodes, num_robots, date = getEvolutionValues()
    best_ebns = []
    connections = []
    bfunctions = []
    ebn = str(num_nodes)
    weibull = 0
    information = 0
    with open(evolution_folder + "/best_individuals_data.txt", "r+") as file:
        try:
            lines = file.readlines()
            for line in lines:
                if line == "\n":
                    best_ebns.append(EBN(connections, bfunctions, ebn, weibull, information))
                    best_ebns[-1].setEBNInfo(num_nodes, num_robots, date)
                    ebn = str(num_nodes)
                    connections = []
                    bfunctions = []
                elif "fitness:" not in line:
                    ebn += line
                    if len(line) == (num_nodes + 1):
                        connections.append(list(map(int, line.strip('\n'))))
                    else:
                        bfunctions.append(list(map(int, line.strip('\n'))))
                elif "fitness:" in line:
                    weibull = int(line.split()[1])
                    information = int(line.split()[2])
            file.close()
        except Exception as e:
            print("Couldnt open paretofront file. Error: " + str(e))

    # best_ebns = sorted(best_ebns, key=operator.attrgetter('ga_weibull_disc'))
    best_ebns = [ebn for ebn in best_ebns if ebn.ga_weibull_disc < min_fitness]
    best_ebns = sorted(best_ebns, key=operator.attrgetter('ga_weibull_disc'))

    if (len(best_ebns) > max_num_of_ebns):
        return best_ebns[:max_num_of_ebns]
    
    return best_ebns

def readBestEBNperGen():
    num_nodes, num_robots, date = getEvolutionValues()
    best_ebns = []
    connections = []
    bfunctions = []
    ebn = str(num_nodes)
    weibull = 0
    information = 0
    jump_ebn = False
    with open(evolution_folder + "/best_individuals_data.txt", "r+") as file:
        try:
            lines = file.readlines()
            for line in lines:
                if line == "\n" and not jump_ebn:
                    best_ebns.append(EBN(connections, bfunctions, ebn, weibull, information))
                    best_ebns[-1].setEBNInfo(num_nodes, num_robots, date)
                    ebn = str(num_nodes)
                    connections = []
                    bfunctions = []
                elif "fitness:" not in line and not jump_ebn:
                    ebn += line
                    if len(line) == (num_nodes + 1):
                        connections.append(list(map(int, line.strip('\n'))))
                    else:
                        bfunctions.append(list(map(int, line.strip('\n'))))
                elif "fitness:" in line:
                    gen = int(line.split()[-1])
                    if (gen % gen_step == 0) or (gen == 1):
                        weibull = int(line.split()[1])
                        information = int(line.split()[2])
                        jump_ebn = False
                    else:
                        jump_ebn = True
            file.close()
        except Exception as e:
            print("Couldnt open paretofront file. Error: " + str(e))

    return best_ebns

def readInitialStates():
    initial_states = []
    with open(evolution_folder + "/initial_state_file.txt", "r+") as init_file:
        try:
            lines = init_file.readlines()
            for line in lines:
                initial_states.append([])
                for ch in line:
                    if ch != '\n':
                        initial_states[-1].append(int(ch))
            init_file.close()
        except Exception as e:
            print("Couldnt open initial states file. Error: " + str(e))
    return initial_states

def createTargetPosition(max_trials):
    targets_pos = []
    target_num = 0
    while(target_num < max_trials):
        theta = random.uniform(-math.pi, math.pi)
        rho = random.uniform(0, arena_radius - 0.025)
        pos_xy = (rho * math.cos(theta), rho * math.sin(theta))
        targets_pos.append(pos_xy)
        target_num += 1

    return targets_pos

### ---------------------------------------------------------------------------------------- ###

def runSimulation(ebn, initial_states, targets_position):
    output = rbn_performance_test.performanceTestEBNfromArguments(str(ebn.num_nodes), ebn.connections, ebn.bfunctions, initial_states, ebn.num_robots,
        targets_position, num_trials, False)
    if (output == None):
        sys.exit("Output has None value")
    return output

def saveBestEBNsResults(ebn):
    with open(posteva_best_ebns_file + "%dR_%dN_%s.txt" % (ebn.num_robots,ebn.num_nodes, ebn.date), "a+") as file:
        try:
            file.write("fitness: %d %d %.2f %d %.2f %d %d\n" % (ebn.weibull_disc, ebn.discovery_time, ebn.fraction_discovery, ebn.information_time,
                ebn.fraction_information, ebn.ga_weibull_disc, ebn.ga_information_time))
            file.close()
        except Exception as e:
            print("Error saving new paretofront: " + str(e))

def saveEBNperGenResults(ebn, gen):
    name_file = posteva_geneneration_ebn_file.replace("posteva", ebn.date + "_posteva")
    with open(name_file + "%dR_%dN.txt" % (ebn.num_robots, ebn.num_nodes), "a+") as file:
        try:
            file.write("fitness: %d %d %.2f %d %.2f %d %d %d\n" % (ebn.weibull_disc, ebn.discovery_time, ebn.fraction_discovery, ebn.information_time,
                ebn.fraction_information, ebn.ga_weibull_disc, ebn.ga_information_time, gen))
            file.close()
        except Exception as e:
            print("Error saving new paretofront: " + str(e))

### ------------------------------------------------------------------------------------------ ###

def evaluateAllBestEBNs():
    best_ebns= readBestEBNs()
    initial_states = readInitialStates()

    print("Starting post-evaluation process of Best EBNs in GA evolution.\nEBNs: %d\nTrials: %d\nFolder: %s" 
        % (len(best_ebns), num_trials, evolution_folder))
    for count, ebn in enumerate(best_ebns):
        print("Starting %d trials test for %d EBN %dN." % (num_trials, (count + 1), ebn.num_nodes))
        random.seed(10)
        targets_position = createTargetPosition(num_trials)
        weibull_disc, disc, frac_disc, inf, frac_inf = runSimulation(ebn, initial_states, targets_position)
        ebn.setResults(weibull_disc, disc, frac_disc, inf, frac_inf)
        saveBestEBNsResults(ebn)

def evaluateEBNsPerGeneration():
    best_ebns = readBestEBNperGen()
    initial_states = readInitialStates()

    print("Starting post-evaluation process of Best EBN per Generation in GA evolution.\nGeneration step: %d\nNumber of trials: %d" % (gen_step, num_trials))
    generation = 1
    for count, ebn in enumerate(best_ebns):
        print("Starting %d trials test for best EBN %dN of %d Generation." % (num_trials, ebn.num_nodes, generation))
        random.seed(10)
        targets_position = createTargetPosition(num_trials)
        weibull_disc, disc, frac_disc, inf, frac_inf = runSimulation(ebn, initial_states, targets_position)
        ebn.setResults(weibull_disc, disc, frac_disc, inf, frac_inf)
        saveEBNperGenResults(ebn, generation)
        generation = (count + 1) *gen_step

def evaluateMultipleEvolutions(evolution_dirs):
    print(evolution_dirs)
    global evolution_folder
    for dir in evolution_dirs:
        evolution_folder = "../Data/Evolution Data/" + dir
        evaluateAllBestEBNs()

# evaluateEBNsPerGeneration()
# evaluateAllBestEBNs()

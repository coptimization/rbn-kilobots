import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits import mplot3d
import csv
import time
from operator import itemgetter
from scipy.optimize import curve_fit
from scipy.ndimage.filters import uniform_filter1d
import os

evolution_paretofront_data = "../Data/2021-09-17_nsga2_10N_fixed_targets/paretofront_individual_data.txt"
post_evaluation_paretofront_data = "../results/paretofront/post_evaluation_paretofront_50N.txt"

num_nodes = 18
num_postevo_trials = 100
num_evo_trials = 8
num_robots = 20

def plotParetoFrontNSGA2():
    paretofront = []
    with open(evolution_paretofront_data, 'r') as data:
        try:
            lines = data.readlines()
            for line in lines:
                if "fitness:" in line:
                    paretofront.append([])
                    paretofront[-1].append(int(line.split()[1]))
                    paretofront[-1].append(int(line.split()[2]))
            data.close()
        except Exception as e:
            print("Couldnt open data file!\n" + str(e))

    order_pareto = sorted(paretofront, key=itemgetter(0))
    disc_weibull = []
    inf_time = []
    for i in order_pareto:
        disc_weibull.append(i[0])
        inf_time.append(i[1])

    plt.plot(disc_weibull, inf_time, alpha=0.6, color="black")
    plt.scatter(disc_weibull, inf_time, color="red")
    plt.grid(b=True, which='major', color='#666666', linestyle='-', alpha=0.5)
    plt.minorticks_on()
    plt.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.2)
    plt.title("NSGA2 - Fronteira de pareto para EBN de %dN" % (num_nodes))
    plt.xlabel("Weibull Discovery Time")
    plt.ylabel("Information Time")
    plt.show()

def plotParetoFrontAfterPostEvaluation():
    paretofront = []
    with open(post_evaluation_paretofront_data, 'r') as data:
        try:
            lines = data.readlines()
            for line in lines:
                paretofront.append([])
                paretofront[-1].append(int(line.split()[1]))
                paretofront[-1].append(int(line.split()[4]))
            data.close()
        except Exception as e:
            print("Couldnt open data file!\n" + str(e))

    order_pareto = sorted(paretofront, key=itemgetter(0))
    disc_weibull = []
    inf_time = []
    for i in order_pareto:
        disc_weibull.append(i[0])
        inf_time.append(i[1])

    # plt.plot(disc_weibull, dist_values, color="black")
    plt.scatter(disc_weibull, inf_time, color="red")
    plt.title("NSGA2 - Fronteira de pareto para EBN de %dN" % (num_nodes))
    plt.xlabel("Weibull Discovery Time")
    plt.ylabel("information Time")
    plt.show()
    
def plotParetoFrontPostEvaluationVsNSGA2():
    post_evaluation_paretofront = []
    evolution_paretofront = []
    with open(post_evaluation_paretofront_data, 'r') as data:
        try:
            lines = data.readlines()
            for line in lines:
                if "fitness:" in line:
                    post_evaluation_paretofront.append([])
                    post_evaluation_paretofront[-1].append(int(line.split()[1]))
                    post_evaluation_paretofront[-1].append(int(line.split()[4]))
                    evolution_paretofront.append([])
                    evolution_paretofront[-1].append(int(line.split()[6]))
                    evolution_paretofront[-1].append(int(line.split()[7]))
            data.close()
        except Exception as e:
            print("Couldnt open data file!\n" + str(e))

    disc_weibull1 = []
    inf_time1 = []
    for i in evolution_paretofront:
        disc_weibull1.append(i[0])
        inf_time1.append(i[1])

    disc_weibull2 = []
    inf_time2 = []
    for i in post_evaluation_paretofront:
        disc_weibull2.append(i[0])
        inf_time2.append(i[1])

    order_pareto = sorted(evolution_paretofront, key=itemgetter(0))
    disc_weibull = []
    inf_time = []
    for i in order_pareto:
        disc_weibull.append(i[0])
        inf_time.append(i[1])

    fig, ax = plt.subplots(figsize=(2, 1))

    ### Plot Pareto Front itself

    plt.plot(disc_weibull, inf_time, color="black", alpha=0.7)
    ax.scatter(disc_weibull1, inf_time1, color="red", label="NSGA2 (k=%d)" % (num_evo_trials))
    ax.scatter(disc_weibull2, inf_time2, color="royalblue", label="Post-evaluation (k=%d)" %(num_postevo_trials))
    ax.legend()
    plt.grid(b=True, which='major', color='#666666', linestyle='-', alpha=0.5)
    plt.minorticks_on()
    plt.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.2)
    plt.title("NSGA2 - Fronteira de pareto e sua Post-evaluation para EBN de N=%d" % (num_nodes))
    plt.xlabel("Weibull Discovery Time (tickets)")
    plt.ylabel("Information Time (tickets)")
    plt.show()

    ### Plot difference in results for each EBN

    all_weibull = []
    for i in range(len(disc_weibull)):
        all_weibull.append([])
        all_weibull[-1].append(evolution_paretofront[i][0])
        all_weibull[-1].append(post_evaluation_paretofront[i][0])
    all_weibull = sorted(all_weibull, key=itemgetter(0))

    index = np.arange(len(disc_weibull))
    width = 0.35

    plt.bar(index, [i[0] for i in all_weibull], width, color="red", label="NSGA2 (k=%d)" % (num_evo_trials))
    plt.bar(index + width, [i[1] for i in all_weibull], width, color="royalblue", label="Post-evaluation (k=%d)" %(num_postevo_trials))
    plt.xticks(index + width / 2, index)
    plt.grid(b=True, which='major', color='#666666', linestyle='-', alpha=0.5)
    plt.minorticks_on()
    plt.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.2)
    plt.title("Mean Weibull Discovery Time para NSAG2 e seu Post-evaluation (EBN de N=%d)" % (num_nodes))
    plt.xlabel("EBNs in Pareto Front")
    plt.ylabel("Weibull Discovery Time (tickets)")
    plt.legend(loc='best')
    plt.show()

def main():
    plotParetoFrontNSGA2()
    plotParetoFrontAfterPostEvaluation()
    plotParetoFrontPostEvaluationVsNSGA2()

main()
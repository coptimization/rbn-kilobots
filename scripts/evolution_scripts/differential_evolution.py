import os
import time
import random
import subprocess
import threading
import sys
from lxml import etree
import math

# Differential Evolution parameters
max_generation = 100
num_population = 50
mutation_factor = 0.3
crossover_rate = 0.5

# Random Boolean Network parameters
num_nodes = 12
num_boolfunction = num_nodes - 1
cn_maxvalue = 2
cn_minvalue = 0
bf_maxvalue = 5
bf_minvalue = 0
min_connections = 3

# Simulation parameters
num_robots = 30
num_threads = 10
num_max_simulation = 5
arena_radius = 0.475
targets_positions = []
min_frac_disc = 0.8

# Path to files 
argos_folder_path = "simulation_config/"
argos_file = "individual_parameters.argos"
argos_file_viz = "individual_parameters_viz.argos"
fitness_folder_path = "src/loop_functions/"
fitness_file_path = "simulation_fitness.txt"
parameters_path = "src/behaviors/parameters_rbn.txt"

class Individual:
    def __init__(self, connections, boolfunctions):
        self.connections = connections
        self.boolfunctions = boolfunctions
        self.discovery_values = []
        self.information_values = []
        self.fraction_disc_values = []
        self.fraction_inf_values = []
        self.num_simulation = 0
        self.fitness = 0

    def getConnections(self):
        return self.connections
    
    def getBoolFunctions(self):
        return self.boolfunctions

    def getFitness(self, disc, inf, frac_d, frac_i):
        self.discovery_values.append(disc)
        self.information_values.append(inf)
        self.fraction_disc_values.append(frac_d)
        self.fraction_inf_values.append(frac_i)
        self.num_simulation += 1

    def calculateAvarege(self):
        self.discovery_time = sum(self.discovery_values)/len(self.discovery_values)
        self.information_time = sum(self.information_values)/len(self.information_values)
        self.fraction_discovery = sum(self.fraction_disc_values)/len(self.fraction_disc_values)
        self.fraction_information = sum(self.fraction_inf_values)/len(self.fraction_inf_values)

    def setFitness(self, fitness):
        self.fitness = fitness

### Pass the individual parameters for the simulation file .c and the id to argos file, initializing it ###
def executeSimulation(individual, id_simu):
    while True:
        with open(parameters_path, "w+") as parameters:
            try:
                parameters.truncate(0)
                parameters.write("%d\n" % num_nodes)
                for i in range(num_nodes):
                    for j in range(num_nodes):
                        parameters.write("%d" % individual.connections[i][j])
                    parameters.write("\n")
                for i in range(num_nodes):
                    for j in range(num_boolfunction):
                        parameters.write("%d" % individual.boolfunctions[i][j])
                    parameters.write("\n")
                parameters.close()
                break
            except:
                print("couldnt open parameters file")
                time.sleep(0.5)

    pos_xy = targets_positions[individual.num_simulation]
    parser = etree.XMLParser(remove_comments = False)
    etree.set_default_parser(parser)
    tree = etree.parse(argos_folder_path + argos_file)
    root = tree.getroot()
    for loop_functions in root.iter('loop_functions'):
        loop_functions.set("id_simulation", "%d" % id_simu)
        loop_functions.set("target_posx", "%.6f" % pos_xy[0])
        loop_functions.set("target_posy", "%.6f" % pos_xy[1])
    id_simu_path = argos_folder_path + str(id_simu) + argos_file
    tree.write(id_simu_path, xml_declaration = True)
    called_argos = False
    while not called_argos:
        try:
            subprocess.Popen(["xdg-open", id_simu_path])
            called_argos = True
        except:
            print("Couldnt call argos simulation")

### Calculate the discovery and information time avarage from each individual ###
def generateIndividualsResults(this_population):
    cont_pop = 0
    simulation_queue = []
    simulation_queue.append(cont_pop)
    print("Executando simulacao %d" % (cont_pop+1))
    executeSimulation(this_population[cont_pop], cont_pop)
    cont_pop += 1
    while(simulation_queue != []):
        if(len(simulation_queue) < num_threads and cont_pop < num_population and os.stat(parameters_path).st_size == 0):
            simulation_queue.append(cont_pop)
            print("Executando simulacao %d" % (cont_pop+1))
            executeSimulation(this_population[cont_pop], cont_pop)
            print(simulation_queue)
            cont_pop += 1
        if(len(simulation_queue) == num_threads or cont_pop >= num_population):
            file_id = -1
            while file_id == -1:
                time.sleep(0.2)
                for i in simulation_queue:
                    if os.path.exists(fitness_folder_path + str(i) + fitness_file_path):
                        file_id = i
                        break
            fitness_file = open(fitness_folder_path + str(file_id) + fitness_file_path, "r")
            disc = int(fitness_file.readline())
            inf = int(fitness_file.readline())
            frac_d = float(fitness_file.readline())
            frac_i = float(fitness_file.readline())
            if disc != 0:
                this_population[file_id].getFitness(disc, inf, frac_d, frac_i)
            fitness_file.close()
            os.remove(fitness_folder_path + str(file_id) + fitness_file_path)
            print("Discovery time :%d Fraction Discovery: %f Information time: %d Fraction Information: %d" % (disc, frac_d, inf, frac_i))
            if this_population[file_id].num_simulation == num_max_simulation:
                simulation_queue.remove(file_id)
                this_population[file_id].calculateAvarege()
                print(simulation_queue)
            else:
                print("Executando simulacao %d" % (file_id+1))
                executeSimulation(this_population[file_id], file_id)

### Calculate the values avarege ###
def avarege(values):
    sum_value = 0.0
    for i in values:
        sum_value += i
    return sum_value/(len(values))

### Calculate the values standard deviation ###
def standardDeviation(values, avarege):
    sum_value = 0.0
    for i in values:
        dif = i - avarege
        sum_value += pow(dif, 2)
    return math.sqrt(sum_value/len(values))

### Normalize the individual parameters ###
def normalization(population, disc_values, inf_values):
    disc_avarege = avarege(disc_values)
    inf_avarege = avarege(inf_values)
    disc_standDevtn = standardDeviation(disc_values, disc_avarege)
    inf_standDevtn = standardDeviation(inf_values, inf_avarege)
    for individual in population:
        ndisc = (individual.discovery_time - disc_avarege)/disc_standDevtn
        ninf = (individual.information_time - inf_avarege)/inf_standDevtn
        fitness = ((ninf + ndisc)/2.0)
        individual.setFitness(fitness)

### Calculate the individuals fitness ###
def calculateFitness(population_1, population_2):
    population = population_1 + population_2
    disc_values = []
    inf_values = []
    for individual in population:
        disc_values.append(individual.discovery_time)
        inf_values.append(individual.information_time)
    normalization(population, disc_values, inf_values)

### Get the best individual from the population ###
def getBestIndividualFitness(this_population):
    fitness = 10
    individual_index = 0
    for i in range(num_population):
        if fitness > this_population[i].fitness:
            fitness = this_population[i].fitness
            individual_index = i
    return this_population[individual_index], individual_index

def getBestIndividualInformationTime(this_population):
    information_time = 100000
    individual_index = 0
    for i in range(num_population):
        if information_time > this_population[i].information_time:
            information_time = this_population[i].information_time
            individual_index = i
    return this_population[individual_index], individual_index

### Create the target position in the arena ###
def createTargetPosition():
    target_num = 0
    while(target_num < num_max_simulation):
        theta = random.uniform(-math.pi, math.pi)
        rho = random.uniform(0,arena_radius)
        pos_xy = (rho * math.cos(theta), rho * math.sin(theta))
        targets_positions.append(pos_xy)
        target_num += 1
    for i in targets_positions:
        print("target positions: %.6f %.6f" % (i[0],i[1]))

### Differential Evolution functions ###
def createPopulation():
    population = []
    for i in range(num_population):
        connections = []
        booleanfunctions = []
        for j in range(num_nodes):
            connections.append([])
            k_cont = 1
            for k in range(num_nodes):
                k_rand = random.randint(0,2)
                if (k_rand == 0):
                    if (k_cont > (num_nodes - min_connections)):
                        k_rand = random.randint(1,2)
                    k_cont += 1
                connections[j].append(k_rand)
        for j in range(num_nodes):
            booleanfunctions.append([])
            for k in range(num_boolfunction):
                booleanfunctions[j].append(random.randint(bf_minvalue, bf_maxvalue))
        individual = Individual(connections, booleanfunctions)
        population.append(individual)
    return population

def verifyBFValue(value):
    if value < bf_minvalue:
        value = bf_minvalue
    elif value > bf_maxvalue:
        value = bf_maxvalue
    return value

def verifyCNValue(value):
    if value < cn_minvalue:
        value = cn_minvalue
    elif value > cn_maxvalue:
        value = cn_maxvalue
    return value

def mutationRand(population):
    mutants = []
    for i in range(num_population):
        rand1 = random.randint(0, num_population - 1)
        rand2 = random.randint(0, num_population - 1)
        while(rand1 == rand2):
            rand2 = random.randint(0, num_population - 1)
        rand3 = random.randint(0, num_population - 1)
        while(rand3 == rand1 or rand3 == rand2):
            rand3 = random.randint(0, num_population - 1)
        connections = []
        booleanfunctions = []
        for j in range(num_nodes):
            connections.append([])
            for k in range(num_nodes):
                value = population[rand1].getConnections()[j][k] - population[rand2].getConnections()[j][k]
                rand_value = random.uniform(0,1)
                if abs(value) == 1 and rand_value < mutation_factor:
                    value = 1
                else:
                    value = 0
                connections[j].append(value)
        for j in range(num_nodes):
            booleanfunctions.append([])
            for k in range(num_boolfunction):
                value = population[rand1].getBoolFunctions()[j][k] + (mutation_factor *(population[rand2].getBoolFunctions()[j][k] - population[rand3].getBoolFunctions()[j][k]))
                booleanfunctions[j].append(verifyBFValue(round(value)))
        individual = Individual(connections, booleanfunctions)
        mutants.append(individual)
    return mutants

def mutationRandToBest(population, best):
    mutants = []
    for i in range(num_population):
        rand1 = random.randint(0, num_population - 1)
        rand2 = random.randint(0, num_population - 1)
        while(rand1 == rand2):
            rand2 = random.randint(0, num_population - 1)
        connections = []
        booleanfunctions = []
        for j in range(num_nodes):
            connections.append([])
            for k in range(num_nodes):
                value = population[i].getConnections()[j][k] + (mutation_factor *(population[best].getConnections()[j][k] - population[i].getConnections()[j][k])) + (mutation_factor *(population[rand1].getConnections()[j][k] - population[rand2].getConnections()[j][k]))
                connections[j].append(verifyCNValue(round(value)))
        for j in range(num_nodes):
            booleanfunctions.append([])
            for k in range(num_boolfunction):
                value = population[i].getBoolFunctions()[j][k] + (mutation_factor *(population[best].getBoolFunctions()[j][k] - population[i].getBoolFunctions()[j][k])) + (mutation_factor *(population[rand1].getBoolFunctions()[j][k] - population[rand2].getBoolFunctions()[j][k]))
                booleanfunctions[j].append(verifyBFValue(round(value)))
        individual = Individual(connections, booleanfunctions)
        mutants.append(individual)
    return mutants

def binomialCrossover(population, mutants):
    childrens = []
    for i in range(num_population):
        connections = []
        booleanfunctions = []
        for j in range(num_nodes):
            connections.append([])
            for k in range(num_nodes):
                rand_value = random.uniform(0,1)
                if rand_value <= crossover_rate:
                    connections[j].append(mutants[i].getConnections()[j][k])
                else:
                    connections[j].append(population[i].getConnections()[j][k])
        for j in range(num_nodes):
            booleanfunctions.append([])
            for k in range(num_boolfunction):
                rand_value = random.uniform(0,1)
                if(rand_value <= crossover_rate):
                    booleanfunctions[j].append(mutants[i].getBoolFunctions()[j][k])
                else:
                    booleanfunctions[j].append(population[i].getBoolFunctions()[j][k])
        individual = Individual(connections, booleanfunctions)
        childrens.append(individual)
    return childrens

def selection(population, childrens):
    new_population = []
    generateIndividualsResults(childrens)
    for i in range(num_population):
        if childrens[i].information_time <= population[i].information_time:
            new_population.append(childrens[i])
        else:
            new_population.append(population[i])      
    return new_population

### Save in a file the best fitness values ###
def savingLearningCurve(individual):
    data = open("learning_curve.txt","a")
    data.write("%d %f %d %f\n" % (individual.discovery_time, individual.fraction_discovery, individual.information_time, individual.fraction_information))
    data.close()

def printBestIndividual(individual):
    print("Best fitness is %f with %d discovery time, %f fraction discovery, %d information time and %F fraction information" % (individual.fitness, 
        individual.discovery_time, individual.fraction_discovery, individual.information_time, individual.fraction_information))

def printPopulation(this_population, gen):
    index = 0
    print("Generation %d:" % (gen))
    for individual in this_population:
        print("Individual %d: %f fitness - %d discovery time - %f fraction discovery - %d information time - %f fraction information" % (index, 
            individual.fitness, individual.discovery_time, individual.fraction_discovery, individual.information_time, individual.fraction_information))
        index += 1

def main():
    random.seed(10)
    initial_state = [1] *num_nodes
    createTargetPosition()
    population = createPopulation()
    generateIndividualsResults(population)
    calculateFitness(population, [])
    best_individual, best_individual_index = getBestIndividualInformationTime(population)
    savingLearningCurve(best_individual)
    generation = 1
    printPopulation(population, generation)
    printBestIndividual(best_individual)
    while generation <= max_generation:
        mutants = mutationRandToBest(population, best_individual_index)
        childrens = binomialCrossover(population, mutants)
        population = selection(population, childrens)
        best_individual, best_individual_index = getBestIndividualInformationTime(population)
        savingLearningCurve(best_individual)
        printPopulation(population, generation)
        printBestIndividual(best_individual)
        generation += 1

# Calls Main loop
main()

import os
import time
import random
import subprocess
import threading
import sys
from lxml import etree
import math
from pymoo.configuration import Configuration
from pymoo.model.problem import Problem
from pymoo.algorithms.nsga2 import NSGA2
from pymoo.factory import get_problem
from pymoo.optimize import minimize
from pymoo.visualization.scatter import Scatter
from pymoo.factory import get_sampling, get_crossover, get_mutation
from pymoo.operators.mixed_variable_operator import MixedVariableSampling, MixedVariableMutation, MixedVariableCrossover
import numpy as np
import autograd.numpy as anp
from scipy.optimize import curve_fit
import scipy.special as sc

#### CONFIGURATION PARAMETERS #####

# Argos Simulation parameters
arena_radius = 0.475
fixed_targets_positions = [(0,0), (0.35,0), (0,0.35), (-0.35,0), (0,-0.35)]

# Path to simulation files 
argos_folder_path = "simulation_config/"
argos_file = "simulation_parameters_viz.argos"
argos_file_viz = "simulation_parameters_viz.argos"
fitness_folder_path = "src/loop_functions/"
fitness_file_path = "simulation_fitness.txt"
parameters_file = "src/behaviors/parameters_rbn.txt"

# Path to results files
best_individuals_data = "best_individuals_data.txt"
evolution_curve_data = "evolution_curve_data.txt"
paretofront_individual_data = "paretofront_individual_data.txt"
population_data = "population.txt"
initial_state_file = "initial_state_file.txt"
log_file = "log_file.txt"

# Flags
LOG_FILE = True

class Experiment:

    def setParameters(self, max_g, n_pop, target, n_threads, n_sim, n_nodes):
        # NSGA2 parameters
        self.max_generation = max_g
        self.num_population = n_pop
        self.targets_type = target
        self.num_max_simulation = n_sim
        self.num_threads = n_threads

        # Argos Simulation parameters
        self.num_robots = 30
        
        # Random Boolean Network parameters
        self.num_nodes = n_nodes
        self.num_boolfunction = self.num_nodes - 1
        self.cn_maxvalue = 2
        self.cn_minvalue = 0
        self.bf_maxvalue = 2
        self.bf_minvalue = 0
        self.num_variables = (self.num_nodes*self.num_nodes) + (self.num_nodes*(self.num_nodes-1))

        # Path to results files
        self.data_dir = "Data/" + time.strftime("%Y-%m-%d") + "_nsga2_" + str(self.num_nodes) + "N_" + self.targets_type + "_targets/"

class Individual:

    def __init__(self):
        self.discovery_values = []
        self.information_values = []
        self.fraction_disc_values = []
        self.fraction_inf_values = []
        self.discovery_robots_values = []
        self.num_simulation = 0

    def setFitnessValues(self, disc, inf, frac_d, frac_i, disc_robot):
        self.discovery_values.append(disc)
        self.information_values.append(inf)
        self.fraction_disc_values.append(frac_d)
        self.fraction_inf_values.append(frac_i)
        self.discovery_robots_values += disc_robot
        self.num_simulation += 1

    def calculateFitness(self):
        self.discovery_time = sum(self.discovery_values)/len(self.discovery_values)
        self.information_time = sum(self.information_values)/len(self.information_values)
        self.fraction_discovery = sum(self.fraction_disc_values)/len(self.fraction_disc_values)
        self.fraction_information = sum(self.fraction_inf_values)/len(self.fraction_inf_values)
        self.weibull_discovery_time = self.calculateWeibullDiscoveryTime()

    def calculateWeibullDiscoveryTime(self):
        fpt = np.asarray(self.discovery_robots_values)
        censored = fpt.size - np.count_nonzero(fpt)
        if censored == 0:
            return self.discovery_time
        fpt = fpt[np.argsort(fpt.reshape(-1))]
        times_value = fpt[censored:].reshape(-1)

        F = self.estimatorKM(times_value, censored)

        # popt_weibull[0] is alpha and popt_weibull[1] is gamma
        bound_is = 75000
        popt_weibull, _ = curve_fit(self.weib_cdf, xdata=times_value, ydata=np.squeeze(F), bounds=(0, [bound_is, 10]), method='trf')
        return sc.gamma(1 + (1. / popt_weibull[1])) * popt_weibull[0]

    def estimatorKM(self, data, censored):
        n_est = np.asarray(range(0, data.size))[::-1] + censored
        RT_sync = []
        for i in range(n_est.size):
            if len(RT_sync) == 0:
                RT_sync.append((n_est[i] - 1) / n_est[i])
            else:
                RT_sync.append(RT_sync[-1] * ((n_est[i] - 1) / n_est[i]))
        F = 1 - np.asarray(RT_sync).reshape(-1, 1)
        return F

    def weib_cdf(self, x, alpha, gamma):
        return (1 - np.exp(-np.power(x / alpha, gamma)))


class MyProblem(Problem):

    def __init__(self, expt):
        self.expt = expt
        cn_xl = self.expt.cn_minvalue * anp.ones(self.expt.num_nodes*self.expt.num_nodes)
        bf_xl = self.expt.bf_minvalue * anp.ones(self.expt.num_nodes*(self.expt.num_nodes-1))
        xl = anp.append(cn_xl, bf_xl)
        cn_xu = self.expt.cn_maxvalue * anp.ones(self.expt.num_nodes*self.expt.num_nodes)
        bf_xu = self.expt.bf_maxvalue * anp.ones(self.expt.num_nodes*(self.expt.num_nodes-1))
        xu = anp.append(cn_xu, bf_xu)
        self.generation = 0
        self.best_fitness = 1000000
        self.createInitialStates()
        self.savingInitialState()
        if (self.expt.targets_type == 'fixed'):
            self.setTargetPosition(fixed_targets_positions) 
        super().__init__(self.expt.num_variables, n_obj=2, n_constr=0, xl=xl, xu=xu, evaluation_of="auto")

    def _evaluate(self, population, out, *args, **kwargs):
        f1 = [0]*self.expt.num_population
        f2 = [0]*self.expt.num_population
        self.LOG("\nStarting Generation %d..." % (self.generation + 1), False)
        if (self.expt.targets_type == 'random'):
            self.createTargetPosition()
        elif(self.expt.targets_type == 'fixed_and_random'):
            self.createTargetPositionWithFixedTarget()
        self.createPopulationFitness()
        cont_pop = 0
        simulation_queue = []
        simulation_queue.append(cont_pop)
        self.executeSimulation(cont_pop, population[cont_pop], self.targets_positions[self.pop_fitness[cont_pop].num_simulation], self.pop_fitness[cont_pop])
        # self.LOG(str(simulation_queue), False)
        cont_pop += 1
        while(simulation_queue != []):
            if(len(simulation_queue) < self.expt.num_threads and cont_pop < self.expt.num_population and not os.path.exists(self.last_ran_file)):
                simulation_queue.append(cont_pop)
                self.executeSimulation(cont_pop, population[cont_pop], self.targets_positions[self.pop_fitness[cont_pop].num_simulation], self.pop_fitness[cont_pop])
                # self.LOG(str(simulation_queue), False)
                cont_pop += 1
            if(len(simulation_queue) == self.expt.num_threads or cont_pop >= self.expt.num_population):
                disc, inf, frac_d, frac_i, disc_r, file_id = self.getSimulationResults(simulation_queue)
                if disc != 0:
                    self.pop_fitness[file_id].setFitnessValues(disc, inf, frac_d, frac_i, disc_r)
                    self.LOG("Simulation %d -> Trial %d is finished!" % (file_id, self.pop_fitness[file_id].num_simulation), False)
                else:
                    self.LOG("Simulation %d -> Trial %d failed! Running again..." % (file_id, self.pop_fitness[file_id].num_simulation), True)
                    time.sleep(1)
                if self.pop_fitness[file_id].num_simulation == self.expt.num_max_simulation:
                    simulation_queue.remove(file_id)
                    self.LOG("Simulation %d is finished!" % (file_id), False)
                    # self.LOG(str(simulation_queue), False)
                    self.pop_fitness[file_id].calculateFitness()
                    f1[file_id] = (self.pop_fitness[file_id].information_time)
                    f2[file_id] = (self.pop_fitness[file_id].weibull_discovery_time)
                else:
                    call_simulation = False
                    while not call_simulation:
                        if not os.path.exists(self.last_ran_file):
                            self.executeSimulation(file_id, population[file_id], self.targets_positions[self.pop_fitness[file_id].num_simulation], self.pop_fitness[file_id])
                            call_simulation = True

        # self.printPopulation()
        self.savingGeneration(population)
        self.savingIndividualEvolutionCurve(population)
        self.generation += 1
        out["F"] = anp.column_stack([f1, f2])

    def setTargetPosition(self, target_pos):
        self.targets_positions = target_pos
        self.LOG("New targets created: ", False)
        for i in self.targets_positions:
            self.LOG(" Target position is (%.6f, %.6f)" % (i[0],i[1]), False)

    def createTargetPosition(self):
        target_num = 0
        self.targets_positions = []
        while(target_num < self.expt.num_max_simulation):
            theta = random.uniform(-math.pi, math.pi)
            rho = random.uniform(0, arena_radius - 0.025)
            pos_xy = (rho * math.cos(theta), rho * math.sin(theta))
            self.targets_positions.append(pos_xy)
            target_num += 1
        self.LOG("New targets created: ", False)
        for i in self.targets_positions:
            self.LOG(" Target position is (%.6f, %.6f)" % (i[0],i[1]), False)
    
    def createTargetPositionWithFixedTarget(self):
        target_num = 0
        self.targets_positions = fixed_targets_positions.copy()
        while(target_num < (self.expt.num_max_simulation - 5)):
            theta = random.uniform(-math.pi, math.pi)
            rho = random.uniform(0, arena_radius - 0.025)
            pos_xy = (rho * math.cos(theta), rho * math.sin(theta))
            self.targets_positions.append(pos_xy)
            target_num += 1
        self.LOG("New targets created: ", False)
        for i in self.targets_positions:
            self.LOG(" Target position is (%.6f, %.6f)" % (i[0],i[1]), False)

    def createPopulationFitness(self):
        self.pop_fitness = []
        for i in range(self.expt.num_population):
            self.pop_fitness.append(Individual())
    
    def createInitialStates(self):
        self.initial_states = []
        for j in range(self.expt.num_robots):
            self.initial_states.append([])
            for k in range(self.expt.num_nodes):
                self.initial_states[j].append(random.randint(0,1))
                    
    def executeSimulation(self, ind_id, ind_parameters, target_pos, ind_states):
        while True:
            with open(parameters_file, "w+") as parameters:
                try:
                    parameters.truncate(0)
                    parameters.write("%d\n" % self.expt.num_nodes)
                    cont_nline = 0
                    for i in range(len(ind_parameters)):
                        parameters.write("%d" % ind_parameters[i])
                        cont_nline += 1
                        if i < self.expt.num_nodes*self.expt.num_nodes:
                            if cont_nline == self.expt.num_nodes:
                                parameters.write("\n")
                                cont_nline = 0
                        else:
                            if cont_nline == (self.expt.num_nodes - 1):
                                parameters.write("\n")
                                cont_nline = 0
                    for i in range(self.expt.num_robots):
                        for j in range(self.expt.num_nodes):
                            parameters.write("%d" % self.initial_states[i][j])
                        parameters.write("\n")
                    parameters.close()
                    break
                except Exception as e:
                    self.LOG("Couldnt open parameters file for %d individual" % (ind_id), True)
                    self.LOG(" " + str(e), False)
                    time.sleep(1)

        parser = etree.XMLParser(remove_comments = False)
        etree.set_default_parser(parser)
        tree = etree.parse(argos_folder_path + argos_file)
        root = tree.getroot()
        for loop_functions in root.iter('loop_functions'):
            loop_functions.set("num_robots", "%d" % self.expt.num_robots)
            loop_functions.set("target_posx", "%.6f" % target_pos[0])
            loop_functions.set("target_posy", "%.6f" % target_pos[1])
            loop_functions.set("id_simulation", "%d" % ind_id)
        id_simu_path = argos_folder_path + str(ind_id) + argos_file
        tree.write(id_simu_path, xml_declaration = True)
        while True:
            try:
                argos_comand = "argos3 -c " + id_simu_path
                subprocess.Popen(["argos3", "-c", id_simu_path], stdout=subprocess.PIPE)
                # subprocess.Popen(["xdg-open", id_simu_path])
                # os.system('gnome-terminal -- ' + argos_comand)
                break
            except Exception as e:
                self.LOG("Couldnt call argos simulation for %d individual" % (ind_id), True)
                self.LOG(" " + str(e), False)
                time.sleep(1)
        
        self.LOG("Running Simulation %d" % (ind_id), False)
        self.last_ran_file = id_simu_path

    def getSimulationResults(self, simulation_queue):
        disc = inf = frac_i = frac_d = 0
        disc_robot = []
        fitness_found = False
        while True:
            for i in simulation_queue:
                if os.path.exists(fitness_folder_path + str(i) + fitness_file_path):
                    file_id = i
                    fitness_found = True
                    break
            if fitness_found:
                break
            else:
                time.sleep(0.2)

        tries = 0
        while True:
            with open(fitness_folder_path + str(file_id) + fitness_file_path, "r") as fitness_file:
                try:
                    disc = int(fitness_file.readline())
                    inf = int(fitness_file.readline())
                    frac_d = float(fitness_file.readline())
                    frac_i = float(fitness_file.readline())
                    lines = fitness_file.readlines()
                    if len(lines) == 30:
                        for line in lines:
                            disc_robot.append(int(line))
                        fitness_file.close()
                        break
                    else:
                        self.LOG("Fitness file of %d individual has %d lines" % file_id, (4 + len(lines)), True)
                        disc = inf = frac_d = frac_i = 0
                        fitness_file.close()
                        time.sleep(1)
                except Exception as e:
                    disc = inf = frac_d = frac_i = 0
                    tries += 1
                    if (tries == 3):
                        self.LOG("Couldnt open fitness file %d individual" % file_id, True)
                        self.LOG(" " + str(e), False)
                        fitness_file.close()
                        break
                    else:
                        time.sleep(1)

        os.remove(fitness_folder_path + str(file_id) + fitness_file_path)
        return disc, inf, frac_d, frac_i, disc_robot, file_id

    def printPopulation(self):
        print("Generation %d:" % (self.generation + 1))
        for i in range(self.expt.num_population):
            print("Individual %d: %d Weibull Discovery time - %d Information time" % (i, self.pop_fitness[i].weibull_discovery_time, self.pop_fitness[i].information_time))

    def printIndividualFitness(self, individual):
        print("Individual fitness is: %d discovery time, %f fraction discovery, %d information time and %F fraction information - %d Weibull discovery time" % (individual.discovery_time,
            individual.fraction_discovery, individual.information_time, individual.fraction_information, individual.weibull_discovery_time))

    def printIndividualParameters(self, ind_parameters, index):
        print("Individual %d:" % (index))
        cont_nline = 0
        for i in range(len(ind_parameters)):
            cont_nline += 1
            if i < self.expt.num_nodes*self.expt.num_nodes:
                if cont_nline == self.expt.num_nodes:
                    print("")
                    cont_nline = 0
            else:
                if cont_nline == (self.expt.num_nodes - 1):
                    print("")
                    cont_nline = 0

    def savingIndividualEvolutionCurve(self, population):
        best_gen_weibull = 100000
        best_idx = 0
        for i in range(len(self.pop_fitness)):
            if best_gen_weibull > self.pop_fitness[i].weibull_discovery_time:
                best_gen_weibull = self.pop_fitness[i].weibull_discovery_time
                best_idx = i

        with open(self.expt.data_dir + evolution_curve_data, "a+") as edata:
            try:
                edata.write("%d %d %f %d %f\n" % (self.pop_fitness[best_idx].weibull_discovery_time, self.pop_fitness[best_idx].discovery_time,
                    self.pop_fitness[best_idx].fraction_discovery, self.pop_fitness[best_idx].information_time, self.pop_fitness[best_idx].fraction_information))
                edata.close()
            except Exception as e:
                self.LOG("Couldnt open evolution curve file for saving %d as best individual from %d gen" % (best_gen_weibull, self.generation+1), True)
                self.LOG(" " + str(e), False)

        if (self.pop_fitness[best_idx].weibull_discovery_time < 30000):
            self.savingBestIndividuals(population[best_idx], self.pop_fitness[best_idx])

    def savingBestIndividuals(self, ind_parameters, ind_fitness):
        num_connections = self.expt.num_nodes*self.expt.num_nodes
        with open(self.expt.data_dir + best_individuals_data, "a+") as idata:
            try:
                idata.write("fitness: %d %d %f %d %f" % (ind_fitness.weibull_discovery_time, ind_fitness.discovery_time, ind_fitness.fraction_discovery,
                    ind_fitness.information_time, ind_fitness.fraction_information))
                for j in range(num_connections):
                    if (j % self.expt.num_nodes == 0):
                        idata.write("\n")
                    idata.write("%d" % ind_parameters[j])
                idata.write("\n")
                cont_line = 0
                for j in range(num_connections, self.expt.num_variables):
                    idata.write("%d" % ind_parameters[j])
                    cont_line += 1
                    if (cont_line == (self.expt.num_nodes-1)):
                        idata.write("\n")
                        cont_line = 0
                idata.write("\n")
                idata.close()
            except Exception as e:
                self.LOG("Problem saving %d as best individual from %d gen in '%s' file" % (ind_fitness[0], self.generation+1, best_individuals_data), True)
                self.LOG(" " + str(e), False)

    def savingGeneration(self, population):
        idata = open(self.expt.data_dir + population_data, "a+")
        idata.truncate(0)
        idata.write("Generation %d:\n" % (self.generation))
        num_connections = self.expt.num_nodes*self.expt.num_nodes
        for i in range(len(population)):
            for j in range(num_connections):
                if (j % self.expt.num_nodes == 0):
                    idata.write("\n")
                idata.write("%d" % population[i][j])
            idata.write("\n")
            cont_line = 0
            for j in range(num_connections, self.expt.num_variables):
                idata.write("%d" % population[i][j])
                cont_line += 1
                if (cont_line == (self.expt.num_nodes-1)):
                    idata.write("\n")
                    cont_line = 0
        idata.close()

    def savingInitialState(self):
        with open(self.expt.data_dir + initial_state_file, "a+") as init_file:
            try:
                init_file.truncate(0)
                for i in self.initial_states:
                    for j in i:
                        init_file.write(str(j))
                    init_file.write("\n")
                init_file.close()
            except Exception as e:
                self.LOG("Couldnt save initial state:", True)
                self.LOG(" " + str(e), False)

    def LOG(self, message, warn):
        print(message)
        if LOG_FILE:
            with open(self.expt.data_dir + log_file, "a+") as log:
                try:
                    log.write(message)
                    if warn:
                        log.write(" ------------------------------------------------------ ERROR")
                    log.write("\n")
                    log.close()
                except Exception as e:
                    print("Couldnt open LOG file!\n" + str(e))


### Main Functions ###
def createDataDir(expt):
    if not os.path.exists(expt.data_dir):
        os.makedirs(expt.data_dir)

def initializeLOG(expt):
    if LOG_FILE:
        with open(expt.data_dir + log_file, "a+") as log:
            try:
                log.truncate(0)
                log.write("NSGA2 - " + str(expt.num_nodes) + " Nodes EBN Simulation ---------------- Start Date: " + time.strftime("%Y-%m-%d") + "\n\n")
                log.close()
            except Exception as e:
                print("Couldnt open LOG file!\n" + e)
    else:
        return

def printParetoFront(pop_fitness):
    print("Pareto front is:")
    for ind in pop_fitness:
        print("%d %d" % (ind[0], ind[1]))

def saveParetoFront(pop_fitness, pop_individual, expt):
    idata = open(expt.data_dir + paretofront_individual_data, "a+")
    idata.truncate(0)
    num_connections = expt.num_nodes*expt.num_nodes
    for i in range(len(pop_individual)):
        idata.write("fitness: %d %d" % (pop_fitness[i][1], pop_fitness[i][0]))
        for j in range(num_connections):
            if (j % expt.num_nodes == 0):
                idata.write("\n")
            idata.write("%d" % pop_individual[i][j])
        idata.write("\n")
        cont_line = 0
        for j in range(num_connections, expt.num_variables):
            idata.write("%d" % pop_individual[i][j])
            cont_line += 1
            if (cont_line == (expt.num_nodes-1)):
                idata.write("\n")
                cont_line = 0
        idata.write("\n")
    idata.close()

def my_callback(algorithm):
    pop = algorithm.res.F
    print(pop)

def run_evolution(max_g, n_pop, target, n_threads, n_sim, n_nodes):
    experiment_parameters = Experiment()
    experiment_parameters.setParameters(max_g, n_pop, target, n_threads, n_sim, n_nodes)

    Configuration.show_compile_hint = False
    createDataDir(experiment_parameters)
    initializeLOG(experiment_parameters)

    random.seed(10)
    problem = MyProblem(experiment_parameters)
    algorithm = NSGA2(pop_size=experiment_parameters.num_population,
                sampling=get_sampling("int_random"),
                crossover=get_crossover("int_sbx", prob=0.75, eta=10),
                mutation=get_mutation("int_pm", prob=0.05,eta=10),
                eliminate_duplicates=True)

    res = minimize(problem, 
                algorithm, 
                ('n_gen', experiment_parameters.max_generation),
                seed = 1,
                erbose=False)

    print("\nEvolution is finished after %d generations!\n" % experiment_parameters.max_generation)
    printParetoFront(res.F)
    saveParetoFront(res.F, res.X, experiment_parameters)
    # Scatter().add(res.F).show()

# Calls experiment
run_evolution(1000, 40, 'fixed', 1, 5, 1)

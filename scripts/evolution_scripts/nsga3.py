import os
import time
import random
import subprocess
import threading
import sys
from lxml import etree
import math
from pymoo.model.problem import Problem
from pymoo.algorithms.nsga3 import NSGA3
from pymoo.factory import get_problem, get_reference_directions
from pymoo.optimize import minimize
from pymoo.visualization.scatter import Scatter
from pymoo.factory import get_sampling, get_crossover, get_mutation
from pymoo.operators.mixed_variable_operator import MixedVariableSampling, MixedVariableMutation, MixedVariableCrossover
import numpy as np
import autograd.numpy as anp


# NSGA3 parameters
max_generation = 50
num_population = 10

# Random Boolean Network parameters
num_nodes = 20
num_boolfunction = num_nodes - 1
num_virtualsensors = 4
cn_maxvalue = 2
cn_minvalue = 0
bf_maxvalue = 2
bf_minvalue = 0
num_variables = (num_nodes*num_nodes) + (num_nodes*(num_nodes-1))

# Simulation parameters
num_robots = 30
num_threads = 1
num_max_simulation = 2
arena_radius = 0.475
targets_positions = []

# Path to files 
argos_folder_path = "simulation_config/"
argos_file = "simulation_parameters.argos"
argos_file_viz = "simulation_parameters_viz.argos"
fitness_folder_path = "src/loop_functions/"
fitness_file_path = "simulation_fitness.txt"
parameters_file = "src/behaviors/parameters_rbn.txt"
paretofront_fitness_data = "Results/nsga3/paretofront_fitness_data.txt"
paretofront_individual_data = "Results/nsga3/paretofront_individual_data.txt"
best_individuals_data = "Results/nsga3/bestindividuals_data.txt"
working_dir = "/home/vinicius/Documents/FURG/COPT/rbn-kilobots"
log_file = "LOG.txt"

class Individual:
    def __init__(self):
        self.discovery_values = []
        self.information_values = []
        self.fraction_disc_values = []
        self.fraction_inf_values = []
        self.num_simulation = 0

    def setFitnessValues(self, disc, inf, frac_d, frac_i):
        self.discovery_values.append(disc)
        self.information_values.append(inf)
        self.fraction_disc_values.append(frac_d)
        self.fraction_inf_values.append(frac_i)
        self.num_simulation += 1

    def calculateWeibullDiscoveryTime(self):
        t_sync = []
        for i in range(self.num_simulation):
            if (self.fraction_disc_values[i] == 1):
                t_sync.append(self.discovery_time[i])
        t_sync.sort()
        num_found = len(t_sync)

    def calculateFitness(self):
        self.discovery_time = sum(self.discovery_values)/len(self.discovery_values)
        self.information_time = sum(self.information_values)/len(self.information_values)
        self.fraction_discovery = sum(self.fraction_disc_values)/len(self.fraction_disc_values)
        self.fraction_information = sum(self.fraction_inf_values)/len(self.fraction_inf_values)

class MyProblem(Problem):

    def __init__(self):
        cn_xl = cn_minvalue * anp.ones(num_nodes*num_nodes)
        bf_xl = bf_minvalue * anp.ones(num_nodes*(num_nodes-1))
        xl = anp.append(cn_xl, bf_xl)
        cn_xu = cn_maxvalue * anp.ones(num_nodes*num_nodes)
        bf_xu = bf_maxvalue * anp.ones(num_nodes*(num_nodes-1))
        xu = anp.append(cn_xu, bf_xu)
        self.generation = 0
        self.createTargetPosition()
        self.createInitialStates()
        super().__init__(num_variables, n_obj=3, n_constr=0, xl=xl, xu=xu, evaluation_of="auto")

    def _evaluate(self, population, out, *args, **kwargs):
        f1 = [0]*num_population
        f2 = [0]*num_population
        f3 = [0]*num_population
        self.createPopulationFitness()
        cont_pop = 0
        simulation_queue = []
        simulation_queue.append(cont_pop)
        self.executeSimulation(cont_pop, population[cont_pop], self.targets_positions[self.pop_fitness[cont_pop].num_simulation], self.pop_fitness[cont_pop])
        self.LOG(str(simulation_queue))
        cont_pop += 1
        while(simulation_queue != []):
            if(len(simulation_queue) < num_threads and cont_pop < num_population and not os.path.exists(self.last_ran_file)):
                simulation_queue.append(cont_pop)
                self.executeSimulation(cont_pop, population[cont_pop], self.targets_positions[self.pop_fitness[cont_pop].num_simulation], self.pop_fitness[cont_pop])
                self.LOG(str(simulation_queue))
                cont_pop += 1
            if(len(simulation_queue) == num_threads or cont_pop >= num_population):
                disc, inf, frac_d, frac_i, file_id = self.getSimulationResults(simulation_queue)
                if disc != 0:
                    self.pop_fitness[file_id].setFitnessValues(disc, inf, frac_d, frac_i)
                    self.LOG("Simulation %d -> Discovery time :%d Fraction Discovery: %f Information time: %d Fraction Information: %f" % (file_id, disc, frac_d, inf, frac_i))
                else:
                    self.LOG("Simulation %d -> Simulation failed! It will run again." % (file_id))
                    time.sleep(1)
                if self.pop_fitness[file_id].num_simulation == num_max_simulation:
                    simulation_queue.remove(file_id)
                    self.LOG(str(simulation_queue))
                    self.pop_fitness[file_id].calculateFitness()
                    f1[file_id] = (self.pop_fitness[file_id].information_time)
                    f2[file_id] = (self.pop_fitness[file_id].discovery_time)
                    f3[file_id] = 1 - (self.pop_fitness[file_id].fraction_discovery)
                else:
                    call_simulation = False
                    while not call_simulation:
                        if not os.path.exists(self.last_ran_file):
                            self.executeSimulation(file_id, population[file_id], self.targets_positions[self.pop_fitness[file_id].num_simulation], self.pop_fitness[file_id])
                            call_simulation = True

        self.printPopulation(self.pop_fitness)
        self.savingBestIndividuals(self.pop_fitness)
        self.createTargetPosition()
        self.generation += 1
        out["F"] = anp.column_stack([f1, f2, f3])

    def createTargetPosition(self):
        target_num = 0
        self.targets_positions = []
        while(target_num < num_max_simulation):
            theta = random.uniform(-math.pi, math.pi)
            rho = random.uniform(0, arena_radius - 0.025)
            pos_xy = (rho * math.cos(theta), rho * math.sin(theta))
            self.targets_positions.append(pos_xy)
            target_num += 1
        for i in self.targets_positions:
            self.LOG("target positions: %.6f %.6f" % (i[0],i[1]))

    def createPopulationFitness(self):
        self.pop_fitness = []
        for i in range(num_population):
            self.pop_fitness.append(Individual())
    
    def createInitialStates(self):
        self.initial_states = []
        for j in range(num_robots):
            self.initial_states.append([])
            for k in range(num_nodes):
                self.initial_states[j].append(random.randint(0,1))

    def createInitialStatesVs(self):
        for ind in self.pop_fitness:
            for j in range(num_robots):
                ind.initial_states.append([])
                for k in range(num_nodes):
                    if k < (num_nodes - num_virtualsensors):
                        ind.initial_states[j].append(random.randint(0,1))
                    else:
                        ind.initial_states[j].append(0)
                    
    def executeSimulation(self, ind_id, ind_parameters, target_pos, ind_states):
        while True:
            with open(parameters_file, "w+") as parameters:
                try:
                    parameters.truncate(0)
                    parameters.write("%d\n" % num_nodes)
                    cont_nline = 0
                    for i in range(len(ind_parameters)):
                        parameters.write("%d" % ind_parameters[i])
                        cont_nline += 1
                        if i < num_nodes*num_nodes:
                            if cont_nline == num_nodes:
                                parameters.write("\n")
                                cont_nline = 0
                        else:
                            if cont_nline == (num_nodes - 1):
                                parameters.write("\n")
                                cont_nline = 0
                    for i in range(num_robots):
                        for j in range(num_nodes):
                            parameters.write("%d" % self.initial_states[i][j])
                        parameters.write("\n")
                    parameters.close()
                    break
                except:
                    self.LOG("Couldnt open parameters file for %d individual" % (ind_id))
                    time.sleep(1)

        parser = etree.XMLParser(remove_comments = False)
        etree.set_default_parser(parser)
        tree = etree.parse(argos_folder_path + argos_file)
        root = tree.getroot()
        for loop_functions in root.iter('loop_functions'):
            loop_functions.set("num_robots", "%d" % num_robots)
            loop_functions.set("target_posx", "%.6f" % target_pos[0])
            loop_functions.set("target_posy", "%.6f" % target_pos[1])
            loop_functions.set("id_simulation", "%d" % ind_id)
        id_simu_path = argos_folder_path + str(ind_id) + argos_file
        tree.write(id_simu_path, xml_declaration = True)
        while True:
            try:
                argos_comand = "argos3 -c " + id_simu_path
                # subprocess.Popen(["xdg-open", id_simu_path])
                os.system('gnome-terminal -- ' + argos_comand)
                break
            except:
                self.LOG("Couldnt call argos simulation for %d individual" % (ind_id))
                time.sleep(1)
        
        self.LOG("Running Simulation %d" % (ind_id))
        self.last_ran_file = id_simu_path

    def getSimulationResults(self, simulation_queue):
        fitness_found = False
        while True:
            for i in simulation_queue:
                if os.path.exists(fitness_folder_path + str(i) + fitness_file_path):
                    file_id = i
                    fitness_found = True
                    break
            if fitness_found:
                break
            else:
                time.sleep(0.2)
        tries = 0
        while True:
            with open(fitness_folder_path + str(file_id) + fitness_file_path, "r") as fitness_file:
                try:
                    disc = int(fitness_file.readline())
                    inf = int(fitness_file.readline())
                    frac_d = float(fitness_file.readline())
                    frac_i = float(fitness_file.readline())
                    fitness_file.close()
                    break
                except:
                    disc = inf = frac_d = frac_i = 0
                    tries += 1
                    if (tries == 3):
                        self.LOG("Couldnt open fitness file %d individual" % file_id)
                        fitness_file.close()
                        break
                    else:
                        time.sleep(1)
        os.remove(fitness_folder_path + str(file_id) + fitness_file_path)
        return disc, inf, frac_d, frac_i, file_id

    def printPopulation(self, pop):
        print("Generation %d:" % (self.generation + 1))
        for i in range(num_population):
            print("Individual %d: %d discovery time - %f fraction discovery - %d information time - %f fraction information" % (i, 
                pop[i].discovery_time, pop[i].fraction_discovery, pop[i].information_time, pop[i].fraction_information))

    def printIndividualFitness(self, individual):
        print("Individual fitness is: %d discovery time, %f fraction discovery, %d information time and %F fraction information" % (individual.discovery_time,
            individual.fraction_discovery, individual.information_time, individual.fraction_information))

    def printIndividualParameters(self, ind_parameters, index):
        print("Individuo %d:" % (index))
        cont_nline = 0
        for i in range(len(ind_parameters)):
            cont_nline += 1
            if i < num_nodes*num_nodes:
                if cont_nline == num_nodes:
                    print("")
                    cont_nline = 0
            else:
                if cont_nline == (num_nodes - 1):
                    print("")
                    cont_nline = 0
 
    def savingBestIndividuals(self, pop):
        for i in range(num_population):
            if (pop[i].discovery_time <= 16000 and pop[i].fraction_discovery >= 0.9):
                data = open(best_individuals_data,"a+")
                data.write("%d %d %f\n" % (pop[i].information_time, pop[i].discovery_time, pop[i].fraction_discovery))
                data.close()

    def LOG(self, message):
        print(message)
        with open(log_file, "a+") as log:
            try:
                log.write("nsga3.py - ")
                log.write(message)
                log.write("\n")
                log.close()
            except:
                print("Couldnt open LOG file!")


### Functions ###
def saveParetoFront(pop_fitness, pop_individual):
    fdata = open(paretofront_fitness_data,"a+")
    fdata.truncate(0)
    for i in range(len(pop_fitness)):
        fdata.write("%d %d %f\n" % (pop_fitness[i][0], pop_fitness[i][1], 1 - pop_fitness[i][2]))
    fdata.close()
    idata = open(paretofront_individual_data, "a+")
    idata.truncate(0)
    num_connections = num_nodes*num_nodes
    for i in range(len(pop_individual)):
        idata.write("%d individual:" % (i+1))
        for j in range(num_connections):
            if (j % num_nodes == 0):
                idata.write("\n")
            idata.write("%d" % pop_individual[i][j])
        idata.write("\n")
        cont_line = 0
        for j in range(num_connections, num_variables):
            idata.write("%d" % pop_individual[i][j])
            cont_line += 1
            if (cont_line == (num_nodes-1)):
                idata.write("\n")
                cont_line = 0
    idata.close()

def my_callback(algorithm):
    pop = algorithm.res.F
    print(pop)

def main():
    random.seed(10)
    problem = MyProblem()
    ref_dirs = get_reference_directions("das-dennis", 3, n_partitions=12)
    algorithm = NSGA3(pop_size=num_population,
                sampling=get_sampling("int_random"),
                crossover=get_crossover("int_sbx", prob=1.0, eta=3.0),
                mutation=get_mutation("int_pm", eta=3.0),
                ref_dirs=ref_dirs,
                eliminate_duplicates=True)

    res = minimize(problem, 
                algorithm, 
                ('n_gen', max_generation),
                seed = 1,
                erbose=False)

    saveParetoFront(res.F, res.X)
    Scatter().add(res.F).show()

# Calls Main loop
main()

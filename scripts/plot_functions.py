import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter
import scipy.stats as stats
import pandas as pd
import seaborn as sns
import numpy as np
import math
import scripts.utils as utils
from scipy import stats
import matplotlib.ticker as mtick
import matplotlib as mpl

from sympy import true

def boxplotComparison(df, n_robots, x_value, y_value, hue):
    # order = list(df[x_value].unique())
    # order.insert(1, order[-1])
    # order.pop(-1)
    # order = utils.sortXLabelPlot(df[hue].unique())
    # order = utils.sortXLabelPlot(df[x_value].unique())
    # order = [df.Name[0], "RBN 18N","RBN 20N", "RBN 22N", "RBN 24N", "RBN 26N", "RBN 30N", "EBN 18N", "EBN 20N", "EBN 22N", "EBN 26N", "EBN 30N"]
    # order = [df.Name[0], "18N Better", "18N Equal", "18N Worse", "20N Better", "20N Equal", "20N Worse", "22N Better", "22N Equal", "22N Worse", "24N Equal", "24N Worse", "26N Worse", "30N Worse"]
    # order = [df.Name[0], "18N Better", "18N Similar", "18N Worse", "20N Better", "20N Similar", "22N Better", "22N Similar", "22N Worse", "26N Better", "26N Similar", "30N Worse"]
    # print(order)
    
    # my_pal = {"LMCRW": "#2076F9", "RBN":"#FFAE4D", "EBN": "springgreen"}
    my_pal = {"LMCRW": "#2076F9", "RBN": sns.color_palette()[1], "EBN": sns.color_palette()[2]}
    # my_pal = dict(zip(order, sns.color_palette("Set1", n_colors=len(order))))
    # my_pal["LMCRW"] = "#2076F9"
    # my_pal["RBN 18N"] = "#FFAE4D"
    # my_pal = {"LMCRW": "#2076F9", "Better": "#5AF97A", "Similar":"#F5FC82", "Worse":"#FFAE4D"}
    # my_pal = ["#2076F9", "#5AF97A", "#E4FF6C", "#FFC36C", "#FF4820", "#D671FE", "#AE99B6"]

    sns.set_style("whitegrid")
    # order=order, palette=my_pal,
    # positions = positions + [range(10.5,25.5, 0.5)]
    ax = sns.boxplot(x=x_value, y=y_value, hue=hue, linewidth=1, palette=my_pal,showfliers=True, data=df)
    # ax = sns.violinplot(x=x_value, y=y_value, hue=hue, data=df)

    medians = df.groupby([x_value])[y_value].median()
    std = df.groupby([x_value])[y_value].std()
    print(std)
    # n_networks = df[x_value].value_counts()/20
    # print(n_networks)
    vertical_offset = df[y_value].median() * 0.1
    for xtick in medians.index:
        # print(xtick)
        # ax.text(xtick, medians[xtick] + vertical_offset, f"n={int(n_networks[xtick])}", horizontalalignment='center', size='medium',color='b',weight='semibold')
        ax.text(xtick, medians[xtick] + vertical_offset, f"{int(medians[xtick])}", horizontalalignment='center', size='small',color='b',weight='semibold')
            
    # ax.set_title("For R=%d, %s"%(n_robots[0], r'$D_{arena}$=90cm'), fontsize=18)
    # ax.set_xlabel("Methods", fontsize=14)
    if y_value == "Fraction Discovery":
        ax.set_ylabel("Fraction Discovery", fontsize=16)
        f = lambda x, pos: f'{x*100:,.0f}%'
        ax.yaxis.set_major_formatter(FuncFormatter(f))
    else:
        ax.set_ylabel(r'$t_f$', fontsize=17)
        # f = lambda x, pos: f'{x/10**3:,.0f}K'

    fig = plt.gcf()
    fig.set_size_inches(5, 5)
    fig.tight_layout()
    plt.legend(loc='best', fontsize=14)
    plt.xticks(fontsize=13)
    plt.yticks(fontsize=13)

    ax.set_xlabel("", fontsize=14)
    # ax.set_ylim(0,4600)
    # plt.xticks([])
    plt.yscale("log")

    plt.show()

def boxplotPairComparison(df, n_robots, x_value, y_value, hue):
    if hue == "Bias":
        my_pal = {"Bias": "springgreen", "No Bias": "darkslategrey"}
    else:
        my_pal = {"direct": "steelblue", "indirect": "slategrey"}

    sns.set_style("whitegrid")
    ax = sns.boxplot(x=x_value, y=y_value, hue=hue, dodge=True, linewidth=1, data=df)

    ax.set_title(f"For R={n_robots} - Arena=45cm")
    ax.set_xlabel("Strategy")    
    if y_value == "Fraction Discovery":
        ax.set_ylabel("Fraction Discovery")
        f = lambda x, pos: f'{x*100:,.0f}%'
        ax.yaxis.set_major_formatter(FuncFormatter(f))
    else:
        ax.set_ylabel("First passage Time" + r' ($t_f$)')

    plt.legend(loc='upper right')
    # plt.tight_layout()
    plt.show()

def boxplotGroupedComparasion(df, n_robots, x_value, y_value, hue):
    sns.set_style("whitegrid")
    ax = sns.catplot(kind='box', x=x_value, y=y_value, hue=hue, dodge=True, linewidth=1, data=df)

    # ax.set_title(f"For R={n_robots} - Time=[750, 3000, 12000] - Bias=None")
    # ax.set_xlabel("Size of Arena (cm)")
    # ax.set_ylabel(r'$t_f$')
    # if y_value == "Fraction Discovery":
    #     ax.set_ylabel("Fraction Discovery")
    #     f = lambda x, pos: f'{x*100:,.0f}%'
    #     ax.yaxis.set_major_formatter(FuncFormatter(f))
    # else:
    #     ax.set_ylabel(r'$t_f$')
        # f = lambda x, pos: f'{x/10**3:,.0f}K'
        
    # plt.legend(loc='best')
    plt.tight_layout()
    # ax.yaxis.set_major_formatter(mtick.PercentFormatter()) 
    # plt.ylim(0,200)
    plt.show()

def boxplotMultiPlot(df, n_robots, x_value, y_value, hue):
    # n_plotrows = 2
    # fig, axs = plt.subplots(nrows=n_plotrows, ncols=1, figsize=(17, 6), sharey=True)

    # for row in range(n_plotrows):    
    #     boxplot = axs[row].boxplot(x=x_value, y=y_value)
    #     axs[row].legend(loc="best", title='R²=')
    #     if row == 0:
    #         axs[row].set_xlim(-0.05,0.5)
    #         axs[row].set_title("20N")
    #     else:
    #         axs[row].set_xlim(0,1)
    #         axs[row].set_title("30N")

    my_pal = {"EBN": "steelblue", "RBN":"orange"}
    g = sns.FacetGrid(df, row="Parameters", aspect=2)
    g.map(sns.boxplot, x_value, y_value, palette=my_pal, linewidth=1, showfliers=True, hue="Strategy", data=df)
    g.add_legend()
    g.set_titles(fontsize=25)
    sns.set_style("whitegrid")

    # # order=order, palette=my_pal,
    # ax = sns.boxplot(x=x_value, y=y_value, hue=hue, linewidth=1, showfliers=True, dodge=False, data=df)
    # # ax = sns.violinplot(x=x_value, y=y_value, hue=hue, data=df)

    fig = plt.gcf()
    fig.set_size_inches(13, 7)
    fig.tight_layout()
    # plt.legend(loc='best', fontsize=14)
    # plt.xticks(fontsize=14)
    # plt.yticks(fontsize=13)
    plt.ylabel(r'$t_f$', fontsize=16)
    plt.xlabel("N=20    N=30", fontsize=15)
    plt.xticks([])
    plt.show()

def barplotGroupedComparison(df, n_robots):
    my_pal = {"Worst": "red", "Equal": "orange", "Better":"springgreen"}

    sns.set_style("whitegrid")
    ax = sns.barplot(x='Name', y='Value', hue="Comparison", palette=my_pal,data=df)

    ax.set_title("For R=%d, %s"%(n_robots, r'$D_{arena}$=90cm') )
    ax.set_xlabel("Strategies", fontsize=15)
    # if y_value == "Fraction Discovery":
    #     ax.set_ylabel("Fraction Discovery", fontsize=15)
    #     f = lambda x, pos: f'{x*100:,.0f}%'
    #     ax.yaxis.set_major_formatter(FuncFormatter(f))
    # else:
    #     ax.set_ylabel("First passage time " + r'($t_f$)', fontsize=15)
    #     # f = lambda x, pos: f'{x/10**3:,.0f}K'

    fig = plt.gcf()
    fig.set_size_inches(12, 5)
    fig.tight_layout()
    plt.legend(loc='best', fontsize=11)
    plt.xticks(fontsize=12)
    plt.show()

def plotCurveComparison(all_data, legends, title, xlabel, ylabel):
    fig = plt.gcf()
    fig.set_size_inches(16, 7)

    for idx, data in enumerate(all_data):
        plt.plot(data, label=legends[idx])

    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    # plt.grid(b=True, which='major', color='#666666', linestyle='-', alpha=0.5)
    # plt.minorticks_on()
    # plt.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.2)
    plt.legend()
    plt.tight_layout()
    plt.show()

def plotDistribution(results1, results2, label1, label2, conclusion, p, bonf):
    fig = plt.subplot()
    plt.plot(results1, label=label1)
    plt.plot(results2, label=label2)
    fig.set_ylabel("First passage Time" + r' ($t_f$)')
    fig.set_title("Distribution comparison: %s (p=%f for bf=%f)" % (conclusion, p, bonf))
    fig.set_xlabel("Experiments") 
    fig.legend(loc="best")
    plt.show()

def violinPlotComparison(results1, results2, label1, label2, conclusion, p, bonf):
    fig = plt.figure()
    pos=[1,2]
    data = [results1, results2]
    ax = fig.add_subplot(111)
    ax.violinplot(data)
    ax.set_xticks([1, 2])
    ax.set_xticklabels([label1, label2])
    # ax.set_xlim([0, 2])
    ax.set_ylabel("First passage Time" + r' ($t_f$)')
    ax.set_title("Distribution comparison: %s (p=%f for bf=%f)" % (conclusion, p, bonf))
    ax.set_xlabel("Experiments") 
    ax.legend(loc="best")
    
    plt.show()


# def add_label(violin, label):
#     color = violin["bodies"][0].get_facecolor().flatten()
#     return (mpatches.Patch(color=color), label)


# def violinPLot(df):
#     strategies = df['Name'].unique()
#     all_fpt_data = []
#     label_name = []
#     for strategy in strategies:
#         fpt_values = df.loc[df['Name'] == strategy]["First Passage Time"].values
#         fpt_values.sort()
#         all_fpt_data.append(fpt_values)
#         label_name.append(df.loc[df['Name'] == strategy]["Strategy"].values[0])

#     labels = []

#     add_label(plt.violinplot(data), label_name)    
#     add_label(plt.violinplot(data), "Linear")
#     add_label(plt.violinplot(data), "Quadratic")

#     plt.legend(*zip(*labels), loc=2)

def violinPlotShowAllDistributionsComparison(df):
    strategies = df['Name'].unique()
    all_fpt_data = []
    label = []
    for strategy in strategies:
        fpt_values = df.loc[df['Name'] == strategy]["First Passage Time"].values
        fpt_values.sort()
        all_fpt_data.append(fpt_values)
        label.append(df.loc[df['Name'] == strategy]["Strategy"].values[0])

    pos = [i for i in range(1, len(strategies)+1)]
    # data = []
    # labels = []
    # for key, results in dict.items():
    #     results.sort()
    #     data.append(results)
    #     labels.append(key)
    print(all_fpt_data)

    fig = plt.figure()
    ax = fig.add_subplot(111)
    # sns.set_palette(palette="ocean")
    # violin_parts = sns.violinplot(data=df, x="Name", y="First Passage Time", hue="Strategy")
    violin_parts = ax.violinplot(all_fpt_data)
    ax.set_xticks(pos)
    ax.set_xticklabels(strategies)
    # ax.set_xlim([0, 2])
    ax.yaxis.grid(True, color ="gray", lw = .5, alpha=0.5)
    ax.set_axisbelow(True)
    ax.set_ylabel("First passage Time" + r' ($t_f$)')
    # ax.set_title("")
    ax.set_xlabel("Strategy")

    labels_foda = []
    import matplotlib.patches as mpatches
    labels_foda.append((mpatches.Patch(color='#F16F08'), "LMCRW"))
    labels_foda.append((mpatches.Patch(color='#2A8038'), "EBN"))
    labels_foda.append((mpatches.Patch(color='#0D2E71'), "RBN"))
    ax.legend(*zip(*labels_foda),loc="best")

    for idx, vp in enumerate(violin_parts['bodies']):
        if label[idx] == "LMCRW":
            vp.set_facecolor('#F16F08')
            vp.set_edgecolor('#b56a07')
            vp.set_linewidth(1)
            vp.set_alpha(0.5)
        elif label[idx] == "EBN":
            vp.set_facecolor('#2A8038')
            vp.set_edgecolor('#062C06')
            vp.set_linewidth(1)
            vp.set_alpha(0.5)
        else:
            vp.set_facecolor('#0D2E71')
            vp.set_edgecolor('#041434')
            vp.set_linewidth(1)
            vp.set_alpha(0.5)

    fig.tight_layout()
    plt.show()

def plotTrajectory(data, title, targets_pos, arena_radius):
    kilobot_comm_radius = 0.02

    fig, ax = plt.subplots(1, figsize=(12, 9))
    positions = data[0, :]
    pos_x, pos_y = zip(*positions)

    arena1 = plt.Circle((0, 0), (arena_radius/100), color='black', fill=False)
    ax.add_patch(arena1)

    ax.scatter(pos_x, pos_y, c="slategrey", s=100)
    ax.scatter(pos_x, pos_y, c=np.arange(len(pos_x)), cmap="magma", s=10)
    if targets_pos != None:
        for target in targets_pos:
            target = plt.Circle((target[0], target[1]), kilobot_comm_radius, edgecolor='black', facecolor='#07F007')
            ax.add_patch(target)

    fig.suptitle("Kilobot trajectory for " + title)
    fig.supylabel("Y axis")
    fig.supxlabel("X axis")
    plt.tight_layout()
    plt.show()

def plotMultiTrajectory(data, title, targets_pos, arena_radius_cm):
    kilobot_comm_radius = 0.02
    arena_radius = (arena_radius_cm/200.0) + 0.025
    max_plots_inline = 5
    len_plots = data.shape[0]
    lines = int(math.ceil(len_plots/max_plots_inline))
    if len_plots >= max_plots_inline:
        n_plots_inline = max_plots_inline
    else:
        n_plots_inline = len_plots

    fig, ax = plt.subplots(lines, n_plots_inline, figsize=(16, 9), sharey=True, sharex=True)
    for line in range(lines):
        for col in range(n_plots_inline):
            idx = (line*n_plots_inline)+col
            if idx < len_plots:
                positions = data[idx, :]
                pos_x, pos_y = zip(*positions)
            else:
                break
            
            arena1 = plt.Circle((0, 0), arena_radius, color='black', fill=False)
            if lines > 1:
                ax[line][col].add_patch(arena1)
                ax[line][col].scatter(pos_x, pos_y, c="slategrey", s=100)
            else:
                ax[col].add_patch(arena1)
                ax[col].scatter(pos_x, pos_y, c="slategrey", s=100)
            
            # offsets = list(zip(pos_x, pos_y))
            # size = np.full((1,len(pos_x)), kilobot_comm_radius)
            # ax.add_collection(EllipseCollection(widths=size, heights=size, angles=0, units='xy',
            #                                facecolors=plt.cm.hsv(5),
            #                                offsets=offsets, transOffset=ax.transData))

            if lines > 1:
                ax[line][col].scatter(pos_x, pos_y, c=np.arange(len(pos_x)), cmap="magma", s=10)
                if targets_pos != None:
                    for target in targets_pos:
                        target = plt.Circle((target[0], target[1]), kilobot_comm_radius, edgecolor='black', facecolor='#07F007')
                        ax[line][col].add_patch(target)
            else:
                ax[col].scatter(pos_x, pos_y, c=np.arange(len(pos_x)), cmap="magma", s=10)
                if targets_pos != None:
                    for target in targets_pos:
                        target = plt.Circle((target[0], target[1]), kilobot_comm_radius, edgecolor='black', facecolor='#07F007')
                        ax[col].add_patch(target)

    fig.suptitle("Kilobot trajectory for " + title)
    fig.supylabel("Y axis")
    fig.supxlabel("X axis")
    plt.tight_layout()
    # plt.show()
    fig.savefig(f"robots-trajec_{title}.png")

def plotSingleGrid(all_grids, title, targets_pos):
    kilobot_comm_radius = 0.02

    fig, ax = plt.subplots(1, figsize=(12, 9))
    sns.heatmap(all_grids[0], ax=ax, yticklabels=False, cbar=False)
    ax.invert_yaxis()

    if targets_pos != None:
        for target in targets_pos:
            target = plt.Circle((target[0], target[1]), kilobot_comm_radius, edgecolor='white', facecolor='red')
            ax.add_patch(target)

    fig.suptitle("Kilobot trajectory grid for " + title)
    fig.supylabel("Y axis")
    fig.supxlabel("X axis")
    plt.tight_layout()
    plt.show()

def plotComparisonNodeStates(nodes, ebns, rbns, max_steps):
    fig = plt.figure(figsize=(7, 4))
    # fig.suptitle('Figure title')
    max_steps = 100

    subfigs = fig.subfigures(1, 2, wspace=0.07)
    for i, subfig in enumerate(subfigs):
        # subfig.suptitle("%dN" % (nodes[i]), fontsize=18, y=0.95)
        axs = subfig.subplots(nrows=1, ncols=2, sharey=True)
        bn = [rbns[i], ebns[i]]
        for j, ax in enumerate(axs):
            generation_1 = []
            node_1 = []
            generation_2 = []
            node_2 = []
            gen = max_steps
            for step in bn[j].node_states[:gen]:
                for n in range(len(step)):
                    if step[n] == 1:
                        node_1.append(n)
                        generation_1.append(gen)
                    else:
                        node_2.append(n)
                        generation_2.append(gen)
                gen -= 1

            generation_1.reverse()
            generation_2.reverse()
            ax.scatter(node_1, generation_1, c="#ffffff", s=10,marker="s")
            ax.set_xticks([])
            # ax.set_yticks([])
            ax.set_ylim(-1,100)
            ax.scatter(node_2, generation_2, c="#3b3b3c", s=10,marker="s")
            ax.set_xlabel("%s" % (bn[j].bn_type), fontsize=13)

    # fig.show()        
    # plt.title("%s %dN" % (rbn.bn_type, rbn.num_nodes))
    # plt.xlabel("Nodes")
    # plt.ylabel("Time Steps (t)")
    # ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    # # plt.xticks(range(math.floor(min(node_1)), math.ceil(max(node_1))+1))
    # fig.subplots_adjust(bottom=0.01)
    # plt.tight_layout()
    plt.gca().invert_yaxis()
    # ax.get_xaxis().set_visible(False)
    plt.show()

def plotMultiGrid(all_grids, title, targets_pos, **kwargs):
    suptitles = kwargs.get('suptitiles')
    kilobot_comm_radius = 0.02
    # max_value_heatmap = max([df.max().max() for df in all_grids])/2
    # min_value_heatmap = min([df.min().min() for df in all_grids])
    # print((max_value_heatmap, min_value_heatmap))
    max_plots_inline = 5
    len_plots = len(all_grids)
    lines = int(math.ceil(len_plots/max_plots_inline))
    if len_plots >= max_plots_inline:
        n_plots_inline = max_plots_inline
    else:
        n_plots_inline = len_plots

    fig, ax = plt.subplots(lines, n_plots_inline, figsize=(16, 9), sharey=True, sharex=True)
    count_grid = 0
    for line in range(lines):
        for col in range(n_plots_inline):
            if count_grid >= len_plots:
                break

            all_grids[count_grid] = utils.standarization(all_grids[count_grid], kilobot_comm_radius)
            sns.heatmap(all_grids[count_grid], ax=ax[line,col], cmap="twilight",yticklabels=False, cbar=False)
            ax[line][col].invert_yaxis()
            if suptitles:
                ax[line][col].set_title(suptitles[count_grid])

            if targets_pos != None:
                for target in targets_pos:
                    target = plt.Circle((target[0], target[1]), kilobot_comm_radius*10, edgecolor='white', facecolor='red')
                    ax[line][col].add_patch(target)
            
            count_grid += 1

    fig.suptitle("Kilobot trajectory grid for " + title)
    fig.supylabel("Y axis")
    fig.supxlabel("X axis")
    plt.tight_layout()
    plt.show()

def plotPerformanceOverBehaviourComparison(all_rbns, all_nodes, bn_type, value_name, show_sl):
    plot_options = dict()
    plot_options["delta"] = ["winter", "Delta " + r'($\Delta$)']
    plot_options["sl"] = ["autumn", "Step Length Normalized"]
    plot_options["complexity"] = ["autumn", "LMC Complexity (C)"]
    plot_options["entropy"] = ["summer", " Shannon Entropy (H)"]
    plot_options["disequilibrium"] = ["spring", "Disequilibrium (D)"]
    plot_options["delta,sl"] = ["spring", "Delta " + r'($\Delta$)' + " and SL normalized"]
    plot_options["entropy,disequilibrium,complexity"] = ["spring", "Entropy, Disequilibrium and Complexity"]
    print(f"\nR-square for {value_name}")

    if len(all_nodes) == 1:
        scatterPlotXOverY(all_rbns[all_nodes[0]], all_nodes[0], show_sl)
        return
    
    encode = ""
    if "," in value_name:
        fig = scatterPlotXOverYMultipleComparison(all_rbns, all_nodes, bn_type, value_name, show_sl, encode, plot_options)
    else:
        fig = scatterPlotXOverYComparison(all_rbns, all_nodes, bn_type, value_name, show_sl, encode, plot_options)

    # fig.supxlabel('Average ' + plot_options[value_name][1], fontsize=14, y=0.0)
    fig.supxlabel('Average Step Length ($\overline{D}$) Average Delta '+ r'($\Delta$)', fontsize=14, y=0.0)
    fig.supylabel(r'$t_f$', fontsize=18, x=0.05)
    # fig.tight_layout()
    # fig.suptitle("For R=20, " + r'$D_{arena}=90cm$', fontsize=14, y=0.95)
    # fig.suptitle("Comparison between performance (fpt) and sensitivity to initial conditions (Delta)")
    plt.show()
    # plt.savefig(f"plot_fptx{value_name}.png")
        
    
def scatterPlotXOverY(results, node, show_sl):
    fig, ax = plt.subplots(figsize=(8, 7))

    if show_sl:
        idx = 0
        x = np.array([delta for delta in results["delta"]], dtype=float)
        y = np.array([(fpt/32) for fpt in results["fpt"]], dtype=float)
        res = stats.linregress(x, y)
        print(f"{node} R-squared: {res.rvalue**2:.6f}")
        z = np.array([sl for sl in results["sl"]], dtype=int)
        max_sl_range = pow(2, node/2)/32
        # z = z/max_sl_range
        scatter = ax.scatter(x, y, c=z, cmap="winter", vmin=0, vmax=max_sl_range, marker='x')
        ax.plot(x, res.intercept + res.slope*x, 'black')
        ax.set_title("RBN %dN" % (node))
        ax.legend(loc="best", title='sl max: %dcm' % (max_sl_range))
        idx += 1

        cbar = plt.colorbar(scatter)
        cbar.set_label('Step Length Range', fontsize=12)
        # fig.colorbar(scatter) 
    else:
        idx = 0
        x = np.array([delta for delta in results["delta"]], dtype=float)
        y = np.array([(fpt/32) for fpt in results["fpt"]], dtype=float)
        res = stats.linregress(x, y)

        scatter = ax.scatter(x, y, color='black', marker='x')
        ax.plot(x, res.intercept + res.slope*x, 'red')
        ax.set_title("RBN %dN" % (node))
        idx += 1

    fig.supxlabel('Normalized SL Average Delta '+ r'($\Delta$)')
    fig.supylabel(r'($t_f$)')
    fig.suptitle("Comparison between performance (fpt) and sensitivity to initial conditions (Delta)")
    fig.tight_layout()   
    plt.show()

def scatterPlotXOverYComparison(all_rbns, all_nodes, bn_type, value_name, show_sl, encode, plot_options):
    fig, axs = plt.subplots(nrows=1, ncols=len(all_nodes), figsize=(15, 3), sharey=True, sharex=True)

    if show_sl:
        idx = 0
        for node in all_nodes:
            label = "%s %d" % (bn_type, node)
            x = np.array([delta for delta in all_rbns[label][value_name]], dtype=float)
            y = np.array([(fpt/32.0) for fpt in all_rbns[label]["fpt"]], dtype=float)
            # y = np.array([fpt for fpt in all_rbns[label]["fpt"]], dtype=float)
            res = stats.linregress(x, y)
            print(f"{label} R-squared: {res.rvalue**2:.6f}")
            z = np.array([sl for sl in all_rbns[label]["sl"]], dtype=float)
            print(z)
            max_sl_range = pow(2, node/2.0)/32.0
            z = z/max_sl_range
            # print(z)
            cmap = plt.get_cmap(plot_options[value_name][0], 5)
            scatter = axs[idx].scatter(x, y, c=z, cmap=cmap, vmin=0, vmax=1, marker='x')
            axs[idx].plot(x, res.intercept + res.slope*x, 'black')
            axs[idx].set_title("%s %dN %s" % (bn_type, node, "encoded" if encode == "encoded" else ""))
            # axs[idx].clim(0,1024)
            # axs[idx].legend(loc='best')
            # axs[idx].grid(True)
            axs[idx].legend(loc="best", title='sl max: %dcm\nR²=%.4f' % (max_sl_range, res.rvalue**2))
            # axs[idx].set_xlim(-0.5,0.5)
            idx += 1

        # Normalizer
        norm = mpl.colors.Normalize(vmin=0, vmax=1)
        ticks_name = ["0-0.2","0.2-0.4","0.4-0.6","0.6-0.8","0.8-1"]
        sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
        sm.set_array([])
        cb_ax = fig.add_axes([0.92, 0.1, 0.02, 0.8])
        cbar = plt.colorbar(scatter, cax=cb_ax, ticks=np.arange(0,1.01,0.2), )
        cbar.set_label('Average Step Length Range' + r' ($\overline{SL}$)', fontsize=11)
        # cbar.ax.set_yticklabels(ticks_name)
        # print(np.arange(0,1.01,0.1))

        # fig.tight_layout()
    else:
        idx = 0
        for node in all_nodes:
            x = np.array([delta for delta in all_rbns[node]["delta"]], dtype=float)
            # y = np.array([(fpt/32) for fpt in all_rbns[node]["fpt"]], dtype=float)
            y = np.array([fpt for fpt in all_rbns[node]["fpt"]], dtype=float)
            res = stats.linregress(x, y)

            scatter = axs[idx].scatter(x, y, color='black', marker='x')
            axs[idx].plot(x, res.intercept + res.slope*x, 'red')
            axs[idx].set_title("%s %dN %s" % (bn_type, node, "encoded" if encode == "encoded" else ""), fontsize=10)
            # axs[idx].clim(0,1024)
            # axs[idx].legend(loc='best')
            # axs[idx].grid(True)
            # axs[idx].set_xlim(-0.5,0.5)
            idx += 1
        
        fig.tight_layout()

    return fig

def scatterPlotXOverYMultipleComparison(all_rbns, all_nodes, bn_type, value_name, show_sl, encode, plot_options):
    value_names = value_name.split(",")
    n_plotrows = len(value_names)
    n_plots_inline = len(all_nodes)
    fig, axs = plt.subplots(nrows=n_plotrows, ncols=n_plots_inline, figsize=(17, 7), sharey=True)
    color_value = ["orange", "green", "blue"]

    for row in range(n_plotrows):
        print(f"\nR-square for {value_name[row]}")
        for col in range(n_plots_inline):            
            label = "%s %d" % (bn_type, all_nodes[col])
            x = np.array([delta for delta in all_rbns[label][value_names[row]]], dtype=float)
            y = np.array([(fpt/32.0) for fpt in all_rbns[label]["fpt"]], dtype=float)
            res = stats.linregress(x, y)
            z = np.array([sl for sl in all_rbns[label]["sl"]], dtype=int)
            max_sl_range = pow(2, all_nodes[col]/2)/32
            # z = z/max_sl_range
            d_arena = 45/max_sl_range
            all_rbns[label]["sl"] = z
            cmap = plt.get_cmap(plot_options[value_names[row]][0], 5)
            # scatter = axs[row][col].scatter(x, y, c=z, cmap=cmap, vmin=0, vmax=1, marker='x')
            axs[row][col].scatter(x, y, color=color_value[row], label="RBN", vmin=0, vmax=1, marker='x')
            print(f"{label} R-squared: {res.rvalue**2:.6f}")
            # if value_names[row] != "sl":
            #     axs[row][col].plot(x, res.intercept + res.slope*x, '#6D5213')
            # else:
            #     axs[row][col].plot(x, res.intercept + res.slope*x, '#093C23')
            # axs[row][col].plot([d_arena]*2, [0,5000], 'black')
            axs[row][col].legend(loc="best", title='R²=%.4f' % (res.rvalue**2))

            # For EBN
            label = "EBN %d" % (all_nodes[col])
            y = np.array([fpt for fpt in all_rbns[label]["fpt"]], dtype=float)
            x_ebn = np.array([delta for delta in all_rbns[label][value_names[row]]], dtype=float)
            res_ebn = stats.linregress(x_ebn, y)

            if row == 0:
                axs[row][col].scatter(x_ebn, y, color="red", label="EBN", vmin=0, vmax=1, marker='x')
                axs[row][col].plot(x, res.intercept + res.slope*x, '#6D5213')
                # axs[row][col].plot(x_ebn, res_ebn.intercept + res_ebn.slope*x_ebn, '#520D03')
                axs[row][col].set_xlim(-0.1,0.5)
                # axs[row][col].legend(loc="best")
                # axs[row][col].set_title("%s %dN %s" % (bn_type, all_nodes[col], "encoded" if encode == "encoded" else ""))
                axs[row][col].set_title("N=%d %s" % (all_nodes[col], "encoded" if encode == "encoded" else ""))
            else:
                axs[row][col].scatter(x_ebn, y, color="blue", label="EBN", marker='x')
                axs[row][col].plot(x, res.intercept + res.slope*x, '#093C23')
                # axs[row][col].plot(x_ebn, res_ebn.intercept + res_ebn.slope*x_ebn, '#06103B')
                # axs[row][col].legend(loc="best", title='sl max: %dcm' % (max_sl_range))
                # axs[row][col].legend(loc="best")
                axs[row][col].set_xlim(0,max_sl_range)

            axs[row][col].legend(loc="best", labels=['RBN', 'EBN', 'R²=%.4f' % (res.rvalue**2)])

            # axs[idx].clim(0,1024)
            # axs[idx].legend(loc='best')
            # axs[idx].grid(True)
            # axs[row][col].legend(loc="best", title='sl max: %dcm' % (max_sl_range))
            # if row == 0:
            #     axs[row][col].set_xlim(0,1)
            # elif row == 1:
            #     axs[row][col].set_xlim(0,1)
            # else:
            #     axs[row][col].set_xlim(0,10)

            # if suptitles:
            #     ax[line][col].set_title(suptitles[count_grid])

        # fig.tight_layout()
        # Normalizer
        # norm = mpl.colors.Normalize(vmin=0, vmax=1)
        # ticks_name = ["0-0.2","0.2-0.4","0.4-0.6","0.6-0.8","0.8-1"]
        # sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
        # sm.set_array([])
        # cb_ax = fig.add_axes([0.92, 0.1, 0.02, 0.8])
        # cbar = plt.colorbar(scatter, cax=cb_ax, ticks=np.arange(0,1.01,0.2), )
        # cbar.set_label('Average Step Length Range' + r' ($\overline{SL}$)', fontsize=11)
        # #cbar.ax.set_yticklabels(ticks_name)
        # print(np.arange(0,1.01,0.1))

    return fig
import sys
import time
import random
import src.KilobotsSearchExperiment as KilobotsSearchExperiment
import src.utils.BooleanNetwork as BooleanNetwork
import src.utils.Targets as Targets
import scripts.log_script as LOG
import os

# Simulattion Parameters
SAVE_POS = True
SAVE_PAR = False
KILOBOT_BIAS = True

# Input files
ebn_parameters_file = "Data/Evolution Data/Best EBNs parameters/single evolution/parameters_ebn_20R_18N.txt"
initial_state_file = "Data/Evolution Data/Best EBNs parameters/single evolution/initial_states_ebn_20R_18N.txt"
multi_ebn_folder = "Data/Evolution Data/Best EBNs parameters/multi-evolution/"
posteva_folder = "Data/Evolution Data/Post-evaluation/GA_Best_EBNs/"
rbn_parameters_folder = "Data/Network Parameters Data/RBNs parameters/"
log_folder = "Data/"

# Output files
weibull_results_folder = "Data/Weibull Discovery time data/"

#### ------------------------------------------------------------------------------------------------------ ####
def readMultiEBNs(num_nodes, num_robots, max_trials, experiment_date):
    parameters_files = []
    init_state_files = []
    for subdir, dirs, files in os.walk(posteva_folder):
        dirs.sort()
        for folder in dirs:
            elements = folder.split("_")
            date = ""
            file_nodes = -1
            file_robots = -1
            for e in elements:
                if e.endswith("N"):
                    file_nodes = int(e.replace("N", ""))
                if e.endswith("R"):
                    file_robots = int(e.replace("R", ""))
                if e.startswith("2023"):
                    date = e  

            if num_nodes != file_nodes or num_robots != file_robots:
                continue

            if not date.startswith(experiment_date):
                continue

            for file_name in os.listdir(posteva_folder + folder):
                if file_name == "initial-states.txt":
                    init_state_files.append(posteva_folder + folder + "/" + file_name)
                if file_name == "parameters_best_ebn.txt":
                    parameters_files.append(posteva_folder + folder + "/" + file_name)

    parameters_files.sort()
    init_state_files.sort()

    ebns = []
    for idx, parameters_file_name in enumerate(parameters_files):
        ebn = BooleanNetwork.BooleanNetwork(num_nodes, bn_type="ebn", read_from=parameters_file_name, net_id=f'{idx:04}')
        ebn.readRobotsInitialStates(num_robots, init_state_files[idx])
        ebn.setPerformanceExperiment(num_robots, max_trials, save_exp=False)
        ebns.append(ebn)

    return ebns

#### ------------------------------------------------------------------------------------------------------ ####
def positioningEvaluationForRBN(num_robots, all_nodes, arena_radius, simulation_time, n_threads):
    num_networks = 5
    max_trials = 1
    num_threads = n_threads

    for num_nodes in all_nodes:
        random.seed(15)
        targets_position = Targets.createTargetPosition(max_trials, False, arena_radius)

        random.seed(15)
        rbns = []
        for count_net in range(num_networks):
            rbn = BooleanNetwork.BooleanNetwork(num_nodes, bn_type="RBN", net_id=f'{count_net:04}')
            rbn.createRobotsInitialStates(num_robots)
            rbn.setPerformanceExperiment(num_robots, max_trials, save_exp=False)
            rbns.append(rbn)

        print("Starting trajectory evaluation for %dN RBN.\nRobots: %d - Arena Radius: %.3fcm\nNetworks: %d - Trials: %d"
            % (num_nodes, num_robots, arena_radius, num_networks, max_trials))
        experiment = KilobotsSearchExperiment.NetworkKilobotsExperiment(num_threads, num_robots, targets_position, arena_radius, simulation_time, KILOBOT_BIAS, SAVE_POS, SAVE_PAR)
        experiment.executeKilobotExperimentTrials(rbns)

def positioningEvaluationForMultiEBNs(num_robots, all_nodes, arena_radius, num_threads):
    max_trials = 10
    experiment_date = "2023"

    for num_nodes in all_nodes:
        ebns = readMultiEBNs(num_nodes, num_robots, max_trials, experiment_date)
        
        random.seed(15)
        targets_position = Targets.createTargetPosition(max_trials, False, arena_radius)
        # targets_position = [(0.38,0), (0,0.38), (-0.38,0), (0,-0.38), (0,0.25), (0.25,0), (0,-0.25), (-0.25,0)]

        print(f"Starting trajectory evaluation for {len(ebns)} seeds of {num_nodes}N EBNs ({num_robots} robots)({max_trials} trials)")
        experiment = KilobotsSearchExperiment.NetworkKilobotsExperiment(num_threads, num_robots, targets_position, arena_radius, KILOBOT_BIAS, SAVE_POS, SAVE_PAR)
        experiment.executeKilobotExperimentTrials(ebns)

def runPositioningEvaluation(num_threads, strategy):
    start_time = time.time()

    arena_radius = 0.925
    simulation_time = 12000
    num_robots = 20
    num_nodes = [18,22,24,26]

    if strategy == "RBN":
        positioningEvaluationForRBN(num_robots, num_nodes, arena_radius, simulation_time, num_threads)
    elif strategy == "EBN":
        positioningEvaluationForMultiEBNs(num_robots, num_nodes, arena_radius, num_threads)

    print("Time running: %s" % (time.time() - start_time))
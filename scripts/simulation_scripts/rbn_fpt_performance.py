import random
import src.KilobotsSearchExperiment as KilobotsSearchExperiment
import src.utils.BooleanNetwork as BooleanNetwork
import src.utils.Targets as Targets
import scripts.log_script as LOG

# Simulation parameters
SAVE_POS = False
SAVE_PAR = False

# Saving folders
log_folder = "Data/"
rbn_parameters_folder = "Data/Network Parameters Data/RBNs parameters/"

def performanceEvaluationUntilNRBNs(n_threads):
    arena_radius = 0.475
    num_robots = 20
    num_nodes = 20
    num_networks = 8000
    num_to_check_results = 1000
    max_trials = 100
    evaluations = 20
    num_threads = n_threads
    best_networks = []
    num_rbns_to_stop = 5
    lmcrw_fpt_value = 980

    log = LOG.LOG(log_folder, "fpt", num_nodes, num_robots, num_networks, num_threads)

    random.seed(15)
    targets_position = Targets.createTargetPosition(max_trials, False, arena_radius)
    random.seed(15)
    rbns = []
    for count_net in range(num_networks):
        rbn = BooleanNetwork.BooleanNetwork(num_nodes, bn_type="RBN", net_id=f'{count_net:04}')
        rbn.createRobotsInitialStates(num_robots)
        rbn.setPerformanceExperiment(num_robots, max_trials, save_exp=False)
        rbns.append(rbn)

    log.write("Starting evaluation for %dN RBN until %d good networks are found.\nRobots: %d - Arena Radius: %.3fcm\nNetworks: %d - Trials: %d"
        % (num_nodes, num_rbns_to_stop, num_robots, arena_radius, num_networks, max_trials), True)
    count_net = 0
    experiment = KilobotsSearchExperiment.NetworkKilobotsExperiment(num_threads, num_robots, targets_position, arena_radius, SAVE_POS, SAVE_PAR)
    while count_net < num_networks:
        start_network_idx = count_net
        end_network_idx = count_net+num_to_check_results
        log.write("\nStarting %d trials for the %d to %d networks:" % (max_trials, start_network_idx, end_network_idx), True)
        experiment.executeKilobotExperimentTrials(rbns[start_network_idx:end_network_idx])

        promising_networks = []
        for rbn in rbns[start_network_idx:end_network_idx]:
            if (rbn.experiment_performance.weibull_discovery_time/32.0) < lmcrw_fpt_value:
                promising_networks.append(rbn)
                rbn.experiment_performance.resetResults()

        log.write(f'{len(promising_networks)} promissing network(s) found! Running {evaluations} evaluations for all...', True)
        if len(promising_networks) > 0:
            for count_eva in range(evaluations):
                log.write("Starting %d trials for the %d best networks (%d evaluation of %d)" % (max_trials, len(promising_networks), (count_eva+1), evaluations), True)
                experiment.executeKilobotExperimentTrials(promising_networks)
                for rbn in promising_networks:
                    rbn.experiment_performance.resetResults()

        for rbn in promising_networks:
            rbn_average_fpt = sum(rbn.experiment_performance.weibull_disc_evaluation_values)/len(rbn.experiment_performance.weibull_disc_evaluation_values)
            if (rbn_average_fpt/32.0) < lmcrw_fpt_value:
                best_networks.append(rbn)
                log.write(f'RBN id:{rbn.net_id} is better than LMCRW! Average fpt: {rbn_average_fpt/32.0}', True)
                rbn.saveNetworkParametersAndPerformance(rbn_parameters_folder)
        log.write(f"Found {len(best_networks)} until now.", True)

        if len(best_networks) >= num_rbns_to_stop:
            count_net += num_to_check_results
            log.write(f'Found {len(best_networks)} good networks after generating {count_net} RBNs.', True)
            break
        else:
            count_net += num_to_check_results
            promising_networks.clear()
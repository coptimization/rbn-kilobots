import sys
import time
import random
import src.KilobotsSearchExperiment as KilobotsSearchExperiment
import src.utils.BooleanNetwork as BooleanNetwork
import src.utils.Targets as Targets
import scripts.log_script as LOG
import os
from operator import itemgetter
import scripts.io_scripts as io_scripts

# Simulation parameters
SAVE_POS = False
SAVE_PAR = False
KILOBOT_BIAS = True

# Input files
ebn_parameters_file = "Data/Evolution Data/Best EBNs parameters/single evolution/parameters_ebn_20R_18N.txt"
initial_state_file = "Data/Evolution Data/Best EBNs parameters/single evolution/initial_states_ebn_20R_18N.txt"
multi_ebn_folder = "Data/Evolution Data/Best EBNs parameters/multi-evolution/"
posteva_folder = "Data/Evolution Data/Post-evaluation/GA_Best_EBNs/"
rbn_parameters_folder = "Data/Network Parameters Data/RBNs parameters/"
log_folder = "Data/"

# Output files
weibull_results_folder = "Data/Weibull Discovery time data/"

#### ------------------------------------------------------------------------------------------------------ ####
def readMultiEBNs(num_nodes, num_robots, max_trials, arena_radius, experiment_date, evolution_targets_type):
    ebn_parameters = []
    for subdir, dirs, files in os.walk(posteva_folder):
        dirs.sort()
        for folder in dirs:
            elements = folder.split("_")
            date = ""
            file_nodes = -1
            file_robots = -1
            file_seed = -1
            file_targets = ""
            for e in elements:
                if e.endswith("N"):
                    file_nodes = int(e.replace("N", ""))
                if e.endswith("R"):
                    file_robots = int(e.replace("R", ""))
                if e.startswith("202"):
                    date = e  
                if e.endswith("seed"):
                    file_seed = int(e.replace("seed", ""))
                if e.endswith("targets"):
                    file_targets = e             

            if num_nodes != file_nodes or num_robots != file_robots:
                continue

            if evolution_targets_type != file_targets:
                continue
        
            # if not date.startswith(experiment_date):
            #     continue

            # if date != "2023-09-18" and date != "2023-10-04" and date != "2023-10-06":
            #     continue

            # if file_seed not in [1,2,4,8,9,10]:
            #     continue

            print(folder)
            ebn_parameters.append(dict())
            ebn_parameters[-1]["date"] = date
            ebn_parameters[-1]["seed"] = file_seed
            ebn_parameters[-1]["target type"] = file_targets
            for file_name in os.listdir(posteva_folder + folder):
                if file_name == "initial-states.txt":
                    ebn_parameters[-1]["initial state"] = posteva_folder + folder + "/" + file_name
                if file_name == "parameters_best_ebn.txt":
                    ebn_parameters[-1]["parameters"] = posteva_folder + folder + "/" + file_name

    ebn_parameters = sorted(ebn_parameters, key=itemgetter('seed'))

    print("Loading EBNs:")
    ebns = []
    for ebn_info in ebn_parameters:
        print(ebn_info["parameters"])
        ebn = BooleanNetwork.BooleanNetwork(num_nodes, bn_type="ebn", read_from=ebn_info["parameters"], net_id=f'{ebn_info["seed"]:04}')
        ebn.readRobotsInitialStates(num_robots, ebn_info["initial state"])
        ebn.setPerformanceExperiment(num_robots, max_trials, round((arena_radius-0.0250)*200), KILOBOT_BIAS, save_exp=True, target_type=f"_id:{ebn_info['target type']}")
        ebns.append(ebn)

    return ebns

#### ------------------------------------------------------------------------------------------------------ ####
def performanceEvaluationForRBN(num_robots, all_nodes, arena_radius, simulation_time, n_threads):
    num_networks = 100
    evaluations = 20
    max_trials = 100
    num_threads = n_threads
    SAVE_PAR = True

    for num_nodes in all_nodes:
        random.seed(15)
        targets_position = Targets.createTargetPosition(max_trials, False, arena_radius)

        random.seed(15)
        rbns = []
        for count_net in range(num_networks):
            rbn = BooleanNetwork.BooleanNetwork(num_nodes, bn_type="RBN", net_id=f'{count_net:04}')
            rbn.createRobotsInitialStates(num_robots)
            rbn.setPerformanceExperiment(num_robots, max_trials, round((arena_radius-0.025)*200), KILOBOT_BIAS, save_exp=True)
            rbns.append(rbn)

        print("Starting performance evaluation for %dN RBN.\nRobots: %d - Arena Radius: %.3fcm\nNetworks: %d - Evaluations: %d - Trials: %d - Bias: %s"
            % (num_nodes, num_robots, arena_radius, num_networks, evaluations, max_trials, KILOBOT_BIAS))
        for count_eva in range(evaluations):
            print("\nStarting %d trials for the %d networks (%d evaluation of %d):" % (max_trials, num_networks, (count_eva+1), evaluations))
            experiment = KilobotsSearchExperiment.NetworkKilobotsExperiment(num_threads, num_robots, targets_position, arena_radius, simulation_time, KILOBOT_BIAS, SAVE_POS, SAVE_PAR)
            experiment.executeKilobotExperimentTrials(rbns)
            for rbn in rbns:
                rbn.experiment_performance.resetResults()

# def performanceEvaluationForEBN(num_robots, num_nodes, arena_radius):
#     num_posteva = 10
#     max_trials = 100

#     ebn = BooleanNetwork.BooleanNetwork(num_nodes, bn_type="ebn", read_from=ebn_parameters_file)
#     ebn.readRobotsInitialStates(num_robots, initial_state_file)
#     ebn.setPerformanceExperiment(num_robots, max_trials, save_exp=True)

#     random.seed(15)
#     all_target_positions = Targets.createAllTargetPositions(num_posteva, max_trials, arena_radius)
#     print("Starting performance evaluation for %dN EBN (%d robots)(%d trials)" % (num_nodes, num_robots, max_trials))
#     for count in range(num_posteva):
#         print("Starting %d trials for the %d time." % (max_trials, (count + 1)))
#         KilobotsSearchExperiment.executeKilobotExperiment(ebn, num_robots, all_target_positions[count], max_trials, arena_radius, SAVE_POS, SAVE_PAR)
#         ebn.resetExperimentResults()

def performanceEvaluationForMultiEBNs(num_robots, all_nodes, arena_radius, simulation_time, num_threads):
    evaluations = 20
    max_trials = 100
    experiment_date = ""
    evolution_targets_type = "random-targets"

    for num_nodes in all_nodes:
        ebns = readMultiEBNs(num_nodes, num_robots, max_trials, arena_radius, experiment_date, evolution_targets_type)
        
        random.seed(15)
        targets_position = Targets.createTargetPosition(max_trials, False, arena_radius)

        print(f"Starting {evaluations} performance evaluations for {len(ebns)} seeds of {num_nodes}N EBNs ({num_robots} robots)({max_trials} trials)")
        for count_eva in range(evaluations):
            print(f"\nStarting {max_trials} trials for {len(ebns)} seeds of {num_nodes} EBNs ({count_eva+1} evaluation of {evaluations})")
            experiment = KilobotsSearchExperiment.NetworkKilobotsExperiment(num_threads, num_robots, targets_position, arena_radius, simulation_time, KILOBOT_BIAS, SAVE_POS, SAVE_PAR)
            experiment.executeKilobotExperimentTrials(ebns)
            for ebn in ebns:
                ebn.experiment_performance.resetResults()

def runFptEvaluation(num_threads, strategy):
    start_time = time.time()

    simulation_total_time = [3000]
    all_arena_radius = [0.475]
    num_robots = [20]
    num_nodes = [28]

    for idx, robots in enumerate(num_robots):
        if strategy == "RBN":
            performanceEvaluationForRBN(robots, num_nodes, all_arena_radius[idx], simulation_total_time[idx], num_threads)
        elif strategy == "EBN":
            performanceEvaluationForMultiEBNs(robots, num_nodes, all_arena_radius[idx], simulation_total_time[idx], num_threads)
        # performanceEvaluationForEBN(num_robots, num_nodes, arena_radius)

    print("Time running: %s" % (time.time() - start_time))
import src.utils.Targets as Targets
import src.utils.BooleanNetwork as BooleanNetwork
import src.KilobotsSearchExperiment as KilobotsSearchExperiment
import random

SAVE_POS = False
SAVE_PAR = False

def main():
    simulation_time = 3000
    arena_radius = 0.475
    num_robots = 20
    num_nodes = 20
    KILOBOT_BIAS = True

    # random.seed(15)
    target_position = Targets.createTargetPosition(1, False, arena_radius)
    rbn = BooleanNetwork.BooleanNetwork(num_nodes, bn_type="RBN", net_id=f'{1:04}')
    rbn.createRobotsInitialStates(num_robots)
    rbn.setPerformanceExperiment(num_robots, 1, round((arena_radius-0.025)*200), KILOBOT_BIAS, save_exp=False)

    experiment = KilobotsSearchExperiment.NetworkKilobotsExperiment(1, num_robots, target_position, arena_radius, simulation_time, KILOBOT_BIAS, SAVE_POS, SAVE_PAR)
    experiment.executeKilobotExperimentTrials(rbn)
#!/bin/sh

if [ $# -eq 0 ]
  then
    echo "No arguments supplied. Please inform the number of threads. (ex: ./install_and_run_exp.sh 96)"
    exit()
fi

chmod +x installation_folder/first_build.sh
chmod +x installation_folder/install_argos.sh

echo "Running install_argos.sh"
source /installation_folder/install_argos.sh
echo "Running first_build.sh"
source /installation_folder/first_build.sh

echo "Number of threads: $1"
echo "Running main.py $1"
python3 main.py $1
